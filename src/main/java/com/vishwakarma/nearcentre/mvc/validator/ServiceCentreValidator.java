package com.vishwakarma.nearcentre.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.vishwakarma.nearcentre.model.ServiceCentre;
import com.vishwakarma.nearcentre.service.ServiceCentreService;


@Component
public class ServiceCentreValidator implements Validator{

	@Resource(name="mvcValidator")
	private Validator validator;
	
	@Resource
	private ServiceCentreService serviceCentreService;
	
	
	@Override
	public boolean supports(Class<?> arg0) {
		
		return ServiceCentre.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ServiceCentre serviceCentre = (ServiceCentre) target;
		validator.validate(serviceCentre, errors);
	}
	
}
