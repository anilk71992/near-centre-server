package com.vishwakarma.nearcentre.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.vishwakarma.nearcentre.model.Product;
import com.vishwakarma.nearcentre.service.ProductService;


@Component
public class ProductValidator implements Validator{

	@Resource(name="mvcValidator")
	private Validator validator;
	
	@Resource
	private ProductService productService;
	
	
	@Override
	public boolean supports(Class<?> arg0) {
		
		return Product.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Product product=(Product) target;
		if(! StringUtils.isBlank(product.getProductName().trim()) && !product.isUpdateOperation())
				{
					Product existingProduct=productService.getProductByName(product.getProductName().trim(),false, null,false,false);
					if(existingProduct!=null )
					{
						errors.rejectValue("productName", "Product with same name already exists");
					}
				}
			
		ValidationUtils.rejectIfEmpty(errors, "productName","Please enter product name... ");
			
		if(! StringUtils.isBlank(product.getProductName().trim()) && !product.isUpdateOperation())
		{
			Product existingProduct=productService.getProductByName(product.getProductName().trim(),false, null,false,false);
			if((existingProduct!=null ) && (existingProduct.getId() != product.getId()))
			{
				errors.rejectValue("productName", "Product with same name already exists");
			}
		}
		validator.validate(product,errors);
	}
	
}
