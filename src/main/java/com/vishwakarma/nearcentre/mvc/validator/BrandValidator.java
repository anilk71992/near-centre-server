package com.vishwakarma.nearcentre.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.Country;
import com.vishwakarma.nearcentre.service.BrandService;





@Component
public class BrandValidator implements Validator{

	@Resource(name="mvcValidator")
	private Validator validator;
	
	@Resource
	private BrandService brandService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		
		return Brand.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Brand brand=(Brand)target;
		
		/*if(! (StringUtils.isBlank(brand.getBrandName().trim()) && !(brand.isUpdateOperation())))
				{
					Brand existingBrand=brandService.getBrandByName(brand.getBrandName().trim(),false,null,false,false);
					
					if(existingBrand != null)
					{
						errors.rejectValue("brandName", "Brand already exists...");
					}
					
				}
		
		ValidationUtils.rejectIfEmpty(errors, "brandName", "Please enter product name...");
		
		if(StringUtils.isBlank(brand.getBrandColor().trim()))
		{
			errors.rejectValue("brandColor", "Please enter brand color...");
		}
		if(!StringUtils.isBlank(brand.getBrandName().trim()) && brand.isUpdateOperation()){
			Brand existingBrand =brandService.getBrandByName(brand.getBrandName().trim(),false,null,false,false);
			if(null != existingBrand &&existingBrand.getId()!=brand.getId()) {
				errors.rejectValue("name", "Country with same name already exists", "Country with same name already exists");
			}
		}*/
		validator.validate(brand, errors);
	}
}
