package com.vishwakarma.nearcentre.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


import com.vishwakarma.nearcentre.api.ApiResponse;
import com.vishwakarma.nearcentre.dto.CityDTO;
import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.City;
import com.vishwakarma.nearcentre.model.ServiceCentre;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.mvc.validator.ServiceCentreValidator;
import com.vishwakarma.nearcentre.property.editor.BrandPropertyEditor;
import com.vishwakarma.nearcentre.property.editor.CityPropertyEditor;
import com.vishwakarma.nearcentre.repository.BrandRepository;
import com.vishwakarma.nearcentre.repository.CityRepository;
import com.vishwakarma.nearcentre.service.CityService;
import com.vishwakarma.nearcentre.service.ServiceCentreService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.CollectionEditor;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class ServiceCentreController {

	@Resource
	private ServiceCentreValidator serviceCentreValidator;

	@Resource
	private ServiceCentreService serviceCentreService;

	@Resource
	private UserService userService;

	@Resource
	private CityService cityService;
	
	@Resource
	CityRepository cityRepository;
	
	@InitBinder(value = "serviceCentre")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(City.class,new CityPropertyEditor(cityRepository));
		
		binder.setValidator(serviceCentreValidator);

	}

	private Model populateModelForAdd(Model model, ServiceCentre serviceCentre) {
		User loggedInUser=userService.getLoggedInUser();
		model.addAttribute("serviceCentre", serviceCentre);
		model.addAttribute("cities",cityService.getAllCities(loggedInUser, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/serviceCentre/add")
	public String addServiceCentre(Model model) {
		model = populateModelForAdd(model, new ServiceCentre());
		return "admin-pages/serviceCentre/add";
	}

	@RequestMapping(value = "/admin/serviceCentre/add", method = RequestMethod.POST)
	public String serviceCentreAdd(ServiceCentre serviceCentre, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		serviceCentreValidator.validate(serviceCentre, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, serviceCentre);
			return "admin-pages/serviceCentre/add";
		}
		serviceCentreService.createServiceCentre(serviceCentre, loggedInUser, true,false);
		String message = "ServiceCentre added successfully";
		return "redirect:/admin/serviceCentre/list?message=" + message;
	}

	@RequestMapping(value = "/admin/serviceCentre/list")
	public String getServiceCentreList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<ServiceCentre> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = serviceCentreService.searchServiceCentre(pageNumber, searchTerm, user,true,false);
		} else {
			page = serviceCentreService.getServiceCentres(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("serviceCentres", page.getContent());

		return "/admin-pages/serviceCentre/list";
	}
	
	@RequestMapping(value="/restapi/value/double")
	@ResponseBody
	public ApiResponse verifyDoubleValue(@RequestParam(required=true)String value)
	{
		ApiResponse apiResponse = new ApiResponse(false);
		try {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException e) {
				apiResponse.setSuccess(true);
			}
		} catch (NearCentreException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	

	@RequestMapping(value = "/admin/serviceCentre/show/{cipher}")
	public String showServiceCentre(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		ServiceCentre serviceCentre = serviceCentreService.getServiceCentre(id, true, loggedIUser,true,false);
		model.addAttribute("serviceCentre", serviceCentre);
		return "/admin-pages/serviceCentre/show";
	}

	@RequestMapping(value = "/admin/serviceCentre/update/{cipher}")
	public String updateServiceCentre(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		ServiceCentre serviceCentre = serviceCentreService.getServiceCentre(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, serviceCentre);
		model.addAttribute("id", serviceCentre.getId());
		model.addAttribute("version", serviceCentre.getVersion());
		return "/admin-pages/serviceCentre/update";
	}

	@RequestMapping(value = "/admin/serviceCentre/update", method = RequestMethod.POST)
	public String serviceCentreUpdate(ServiceCentre serviceCentre, 
										BindingResult formBinding, 
										Model model) {

		serviceCentre.setUpdateOperation(true);
		serviceCentre.setId(Long.parseLong(model.asMap().get("id") + ""));
		serviceCentre.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		serviceCentreValidator.validate(serviceCentre, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, serviceCentre);
			return "admin-pages/serviceCentre/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		serviceCentreService.updateServiceCentre(serviceCentre.getId(), serviceCentre, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "ServiceCentre updated successfully";
		return "redirect:/admin/serviceCentre/list?message=" + message;
	}

	
	@RequestMapping(value = "/admin/serviceCentre/delete/{cipher}")
	public String deleteServiceCentre(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			serviceCentreService.deleteServiceCentre(id);
			message = "ServiceCentre deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete ServiceCentre";
		}
		return "redirect:/admin/serviceCentre/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
	
	@RequestMapping(value="/restapi/existingCities/list")
	@ResponseBody
	public ApiResponse getCities(@RequestParam(required= true) String cityId) {
		ApiResponse apiResponse =new ApiResponse(true);
		try
		{
			Long id=Long.parseLong(cityId);
			List<City> cityList=cityService.getCityByStateName(id, null, false, true);
			List<CityDTO> cityDTOs=new ArrayList<CityDTO>();
			for(City city : cityList) {
				cityDTOs.add(new CityDTO(city));
			}
			apiResponse.addData("existingCities", cityDTOs);
		}
		catch(NearCentreException e)
		{
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	
} 
