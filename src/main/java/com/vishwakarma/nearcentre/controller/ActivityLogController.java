package com.vishwakarma.nearcentre.controller;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.nearcentre.model.ActivityLog;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.property.editor.UserPropertyEditor;
import com.vishwakarma.nearcentre.repository.UserRepository;
import com.vishwakarma.nearcentre.service.ActivityLogService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class ActivityLogController {

	@Resource
	private ActivityLogService activityLogService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	
	@InitBinder(value = "activityLog")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
	}


	@RequestMapping(value = "/admin/activityLog/list")
	public String getActivityLogList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<ActivityLog> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = activityLogService.searchActivityLogs(pageNumber, searchTerm, user,true,false);
		} else {
			page = activityLogService.getActivityLogs(pageNumber, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("activityLogs", page.getContent());

		return "/admin-pages/activitylog/list";
	}

	@RequestMapping(value = "/admin/activityLog/show/{cipher}")
	public String showActivityLog(@PathVariable String cipher, Model model) {
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		ActivityLog activityLog = activityLogService.getActivityLog(id, true, loggedIUser,true,false);
		model.addAttribute("activityLog", activityLog);
		return "/admin-pages/activitylog/show";
	}
	
}
