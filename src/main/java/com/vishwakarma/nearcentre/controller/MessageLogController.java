package com.vishwakarma.nearcentre.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.MessageLog;
import com.vishwakarma.nearcentre.repository.MessageLogRepository;
import com.vishwakarma.nearcentre.repository.UserRepository;
import com.vishwakarma.nearcentre.service.MessageLogService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.AppFileUtils;
import com.vishwakarma.nearcentre.utils.AsyncJobs;
import com.vishwakarma.nearcentre.utils.PlivoSMSHelper;

@Controller
public class MessageLogController {

	@Resource 
	private MessageLogRepository messageLogRepository;
	
	@Resource
	private MessageLogService messageLogService;

	@Resource 
	private UserRepository userRepository;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private UserService userService;
	
	@Resource AsyncJobs asyncJobs;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	@Resource
	private Environment environment;
	
	private static final int PAGE_SIZE = 50;
	
	@RequestMapping("/messagelog/list")
	public String messageLogList(final Model model,
			@RequestParam(defaultValue="1") Integer pageNumber) {
		Page<MessageLog> page = messageLogService.getScanLogs(pageNumber, PAGE_SIZE);
	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + PAGE_SIZE, page.getTotalPages());

	    model.addAttribute("page", page);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("currentIndex", current);
	    model.addAttribute("messageLogs", page.getContent());
	    return "/messagelog/list";
	}

	@RequestMapping("/admin/message/sendMessage")
	public String sendMessage(Model model) {
		
		return "/admin-pages/message/send";
	}
	
	@RequestMapping("/admin/message/list")
	public String sendMessageList(Model model) {
		
		return "/admin-pages/message/list";
	}
	
	@RequestMapping(value = "/admin/message/sendMessage", method = RequestMethod.POST)
	public String messageSend(Model model,
			@RequestParam(required = true) MultipartFile file,
			 @RequestParam(required=false,value="notificationMessage")String notificationMessage)throws InvalidFormatException, IOException {
		
		String message = "sucessfully Send Message";
		List<String> mobileNumberLists=new ArrayList<String>();
		try {
			mobileNumberLists=importedFile(file,notificationMessage);
		} catch (NearCentreException e) {
			e.printStackTrace();
			message = "Error while uploading file,please upload valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/admin/message/list";
		}
		model.addAttribute("mobileNumberLists", mobileNumberLists);
		model.addAttribute("message", message);
		return "/admin-pages/message/list";
	}

	private List<String> importedFile(MultipartFile file,String notificationMessage)
			throws InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(file.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		List<String> mobileNumberList=new ArrayList<String>();
		int rowCount=0;
		for(Row row: sheet){
			
			if(rowCount==0){
				rowCount++;
				continue;
			}
			
			String MobileNumber = null;
			try{
			
			if(null != row.getCell(0)){
				Cell MobileNumberCell = row.getCell(0);
				MobileNumber = MobileNumberCell.getStringCellValue();
				mobileNumberList.add(MobileNumber);
				plivoSMSHelper.sendSMS(MobileNumber, notificationMessage);
			}
			
			}catch(NearCentreException e){
				e.printStackTrace();
				continue;
			}
			
			
			 rowCount++;
		}
		
		return mobileNumberList;
	}
}
