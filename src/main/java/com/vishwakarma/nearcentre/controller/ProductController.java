package com.vishwakarma.nearcentre.controller;


import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.Product;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.mvc.validator.ProductValidator;
import com.vishwakarma.nearcentre.property.editor.BrandPropertyEditor;
import com.vishwakarma.nearcentre.repository.BrandRepository;
import com.vishwakarma.nearcentre.service.BrandService;
import com.vishwakarma.nearcentre.service.ProductService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.CollectionEditor;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class ProductController {

	@Resource
	private ProductValidator productValidator;

	@Resource
	private ProductService productService;

	@Resource
	private UserService userService;
	
	@Resource
	private BrandService brandService;
	
	@Resource
	BrandRepository brandRepository;
	
	@InitBinder(value = "product")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.setValidator(productValidator);
		binder.registerCustomEditor(Set.class, new CollectionEditor(Set.class, brandRepository));
		binder.registerCustomEditor(Brand.class, new BrandPropertyEditor(brandRepository));
	
	}

	private Model populateModelForAdd(Model model, Product product) {
		User loggedInUser=userService.getLoggedInUser();
		model.addAttribute("product", product);
		model.addAttribute("brands",brandService.getAllBrands(loggedInUser, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/product/add")
	public String addProduct(Model model) {
		model = populateModelForAdd(model, new Product());
		return "admin-pages/product/add";
	}

	@RequestMapping(value = "/admin/product/add", method = RequestMethod.POST)
	public String productAdd(Product product, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		productValidator.validate(product, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, product);
			return "admin-pages/product/add";
		}
		productService.createProduct(product, loggedInUser, true,false);
		String message = "Product added successfully";
		return "redirect:/admin/product/list?message=" + message;
	}

	@RequestMapping(value = "/admin/product/list")
	public String getProductList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<Product> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = productService.searchProduct(pageNumber, searchTerm, user,true,false);
		} else {
			page = productService.getProducts(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("products", page.getContent());

		return "/admin-pages/product/list";
	}

	@RequestMapping(value = "/admin/product/show/{cipher}")
	public String showProduct(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Product product = productService.getProduct(id, true, loggedIUser,true,false);
		model.addAttribute("product", product);
		return "/admin-pages/product/show";
	}

	@RequestMapping(value = "/admin/product/update/{cipher}")
	public String updateProduct(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Product product = productService.getProduct(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, product);
		model.addAttribute("id", product.getId());
		model.addAttribute("version", product.getVersion());
		return "/admin-pages/product/update";
	}

	@RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
	public String productUpdate(Product product, 
										BindingResult formBinding, 
										Model model) {

		product.setUpdateOperation(true);
		product.setId(Long.parseLong(model.asMap().get("id") + ""));
		product.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		productValidator.validate(product, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, product);
			return "admin-pages/product/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		productService.updateProduct(product.getId(), product, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Product updated successfully";
		return "redirect:/admin/product/list?message=" + message;
	}

	@RequestMapping(value = "/admin/product/delete/{cipher}")
	public String deleteProduct(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			productService.deleteProduct(id);
			message = "Product deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Product";
		}
		return "redirect:/admin/product/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
}
