package com.vishwakarma.nearcentre.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CharacterEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.nearcentre.api.ApiResponse;
import com.vishwakarma.nearcentre.dto.UserDTO;
import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.messaging.EmailHelper;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.model.User.ReferredMedia;
import com.vishwakarma.nearcentre.mvc.validator.UserValidator;
import com.vishwakarma.nearcentre.property.editor.UserPropertyEditor;
import com.vishwakarma.nearcentre.repository.RoleRepository;
import com.vishwakarma.nearcentre.repository.UserRepository;
import com.vishwakarma.nearcentre.service.ActivityLogService;
import com.vishwakarma.nearcentre.service.CityService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.AsyncJobs;
import com.vishwakarma.nearcentre.utils.CollectionEditor;
import com.vishwakarma.nearcentre.utils.DateHelper;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;
import com.vishwakarma.nearcentre.utils.NotificationHelper;
import com.vishwakarma.nearcentre.utils.PlivoSMSHelper;
import com.vishwakarma.nearcentre.utils.RoleCache;

/**
 * @author Vishal
 *
 */
@Controller
@SessionAttributes(value={"id", "version", "retUrl", "retPage"})
public class UserController {
	
	@Resource
	private UserValidator userValidator;
	
	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private RoleCache roleCache;
	
	@Resource
	private RoleRepository roleRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource 
	private AsyncJobs asyncJobs;
	
	@Resource
	private CityService cityService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private ActivityLogService activityLogService; 
	
	@InitBinder(value="user")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), false);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(Character.class, new CharacterEditor(false));
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Set.class, new CollectionEditor(Set.class, roleRepository));
		
		binder.setValidator(userValidator);
	}
	
	private Model populateModelForAdd(Model model, User user) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("user", user);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("cities", cityService.getAllCities(loggedInUser, false, false));
		return model;
	}
	
	@RequestMapping(value="/admin/user/add")
	public String addCustomer(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		model = populateModelForAdd(model, new User());
		return "admin-pages/user/add";
	}

	@RequestMapping(value="/admin/user/add", method=RequestMethod.POST)
	public String userAdd(User user, BindingResult formBinding, Model model) {
		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "admin-pages/user/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		userService.createUser(user, loggedInUser, false);
		String message = "User added successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/admin/user/list")
	public String getUserList(Model model,
										@RequestParam(defaultValue="1") Integer pageNumber,
										@RequestParam(required=false) String message,
										@RequestParam(required=false) String searchTerm) {
		
		User user = userService.getLoggedInUser();
		model.addAttribute("message", message);
		Page<User> page = null;

		if(StringUtils.isNotBlank(searchTerm)) {
			page = userService.searchUsers(pageNumber, searchTerm, null, user);
		} else {
			page = userService.getUsers(pageNumber, null, user);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("users", page.getContent());

		return "/admin-pages/user/list";
	}
	
	@RequestMapping(value="/admin/user/show/{cipher}")
	public String showCustomer(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		User user = userService.getUser(id, true, loggedIUser);
		model.addAttribute("user", user);
		return "/admin-pages/user/show";
	}
	
	@RequestMapping(value="/admin/user/show")
	public String showCustomerBySearch(@RequestParam String customerCode,Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		User user = userService.getUserByCode(customerCode, false, loggedInUser);
		
		model.addAttribute("user", user);
		return "/admin-pages/user/show";
	}
	
	@RequestMapping(value="/admin/user/update/{cipher}")
	public String updateUser(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User user = userService.getUser(id, true, loggedInUser);
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		return "/admin-pages/user/update";
	}
	
	@RequestMapping(value="/admin/user/update", method=RequestMethod.POST)
	public String userUpdate(User user, BindingResult formBinding, Model model) {
		
		user.setUpdateOperation(true);
		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "/admin-pages/user/update";
		}
		user.setId(Long.parseLong(model.asMap().get("id") + ""));
		user.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		userService.updateUser(user.getId(), user, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		
		String message = "User updated successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/admin/user/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		User user = userService.getUser(id, false);
		String message = "";
		try {
			userService.deleteUser(id, userService.getLoggedInUser());
			message = "User deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete user";
		}
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/restapi/getProfilePicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProfilePic( String email, HttpServletResponse httpServletResponse){
		
		User user = userService.getUserByEmail(email, false);
		File file = new File(user.getProfilePicUrl());
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "image/*");
		httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param email
	 * @return ApiResponse true and sends email to given emailId
	 * @author Vishal
	 */
	@RequestMapping(value="/forgotPassword/restapi")
	@ResponseBody
	public ApiResponse forgotPasswordApi(@RequestParam(required=true) String email) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User user = userService.getUserByEmail(email, true);
			String message = notificationHelper.getEmailBodyForPasswordReset(user, "");
			emailHelper.sendSimpleEmail(user.getEmail(), "NearCentre - Change password request", message);
		} catch (NearCentreException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * Update user
	 * @param userDTO
	 * @param profilePic
	 * @return UserDTO Object 
	 */
	
	@RequestMapping(value="/restapi/user/update",method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateUser(@RequestPart(required=true)UserDTO userDTO,
			@RequestPart(required = false, value = "profilePic") MultipartFile profilePic) {
		
		ApiResponse apiResponse= new ApiResponse(true);
		
		try{
			
			User loggedInUser= userService.getSuperUser();
			User user=userService.getUserByCode(userDTO.getCode(), true, loggedInUser);
			
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			if(null != userDTO.getDob() && !userDTO.getDob().isEmpty()){
				user.setDob(DateHelper.parseDate(userDTO.getDob()));
			}
			user.setFbId(userDTO.getFbId());
			user.setGender(userDTO.getGender());
			user.setgPlusId(userDTO.getgPlusId());
			user.setIsEnabled(userDTO.getIsEnabled());
			user.setIsVerified(userDTO.getIsVerified());
			user.setMobileNumber(userDTO.getMobileNumber());
			user.setAlterNateMobileNumber(userDTO.getAlterNateMobileNumber());
			user.setTwitterId(userDTO.getTwitterId());
			user.setEmail(userDTO.getEmail());
			user.setVerificationCode(userDTO.getVerificationCode());
			
			if(userDTO.getLat()!=0)
				user.setLat(userDTO.getLat());
			
			if(userDTO.getLng()!=0)
				user.setLng(userDTO.getLng());
			
			if(null!=userDTO.getReferredMedia())
				user.setReferredMedia(ReferredMedia.valueOf(userDTO.getReferredMedia()));
			
			if (null != profilePic) {
				try {
					user.setProfilePicData(profilePic.getBytes());
					user.setFileName(profilePic.getOriginalFilename());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			user.setUpdateOperation(true);
						
			BindingResult bindingResult = new DataBinder(user).getBindingResult();
			userValidator.validate(user, bindingResult);
			if(bindingResult.hasErrors()) {
				String errorStr= null;
				for (ObjectError objectError : bindingResult.getAllErrors()) {
					if(StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
					}else{
						errorStr = errorStr+ ", "+ objectError.getDefaultMessage();
					}
					
				}
					throw new NearCentreException(errorStr, "400");
				
			}
			user = userService.updateUser(user.getId(), user, loggedInUser,false,false);
			apiResponse.addData("user", new UserDTO(user));
		} catch(NearCentreException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		
		return apiResponse;
	}
	
	/**
	 * 
	 * @param mobileNumber
	 * @return sends OTP to given mobileNumber
	 * @author Vishal
	 */
	@RequestMapping(value="/restapi/customer/sendOTP", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse sendOTP(@RequestPart(required=true) String mobileNumber,
								@RequestPart(required=false) String referralCode){
		return null;
	}
	
	/*
	 * Update lattitude and longitude of deliveryBoy's current location. 
	 */
	@RequestMapping(value="/restapi/user/updateLatLng", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateLatLng(@RequestParam(required=true) Long deliveryBoyId,
									@RequestParam(required=true)Double lat,
									@RequestParam(required=true)Double lng){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User deliveryBoy = userService.getUser(deliveryBoyId, true);
			deliveryBoy.setLat(lat);
			deliveryBoy.setLng(lng);
			deliveryBoy= userService.updateUser(deliveryBoy.getId(), deliveryBoy, null, false, false);
			apiResponse.addData("deliveryBoy", new UserDTO(deliveryBoy));
			
		}catch(NearCentreException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(),e.getErrorCode());
		}
		
		return apiResponse; 
	}
	
	@RequestMapping(value="/restapi/user/getAllCustomers")
	@ResponseBody
	public ApiResponse getAllCustomers(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser);
			List<UserDTO>dtos = new ArrayList<UserDTO>();
			for (User user : customers) {
				dtos.add(new UserDTO(user));
			}
			apiResponse.addData("customers", dtos);
			
		}catch(NearCentreException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/restapi/user/getDetailsByMobileNumber", method = RequestMethod.GET)
	@ResponseBody
	public ApiResponse getUserDetailsByMobileNumber(@RequestParam(required = true) String mobileNumber) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			if(mobileNumber.length()>10){
				mobileNumber=mobileNumber.substring(mobileNumber.length()-10, mobileNumber.length());
			}
			User user = userService.getUserByMobileNumber(mobileNumber, false);
			if(user != null){
			apiResponse.addData("user", new UserDTO(user));
			} else {
				apiResponse.setSuccess(false);
			}
		} catch (NearCentreException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
}
