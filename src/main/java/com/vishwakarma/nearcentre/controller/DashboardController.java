package com.vishwakarma.nearcentre.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.amazonaws.services.apigateway.model.Model;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.service.PickListItemService;
import com.vishwakarma.nearcentre.service.UserService;

@Controller
public class DashboardController {

	@Resource
	private PickListItemService pickListItemService;

	@Resource
	private UserService userService;
	
	@RequestMapping("/home")
	public String dashboard(final Model model,HttpSession httpSession,@RequestParam(required=false) String message, HttpServletResponse response)
	{
		User user =userService.getLoggedInUser();
		if(StringUtils.isBlank(message))
		{
			return "redirect:/admin/home";
			
		}
		else
		{
			return "redirect:/admin/home?message"+message;

			
		}
		
	}
	
	@RequestMapping("admin/home")
	public String adminHome(Model model)
	{
		User loggedInUser =userService.getLoggedInUser();
		return "admin-pages/home";
	}
	
}
