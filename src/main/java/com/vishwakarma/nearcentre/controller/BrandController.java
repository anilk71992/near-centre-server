package com.vishwakarma.nearcentre.controller;


import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.mvc.validator.BrandValidator;
import com.vishwakarma.nearcentre.service.BrandService;
import com.vishwakarma.nearcentre.service.UserService;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class BrandController {

	@Resource
	private BrandValidator brandValidator;

	@Resource
	private BrandService brandService;

	@Resource
	private UserService userService;
	
	@InitBinder(value = "brand")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.setValidator(brandValidator);
	}

	private Model populateModelForAdd(Model model, Brand brand) {
		model.addAttribute("brand", brand);
		return model;
	}

	@RequestMapping(value = "/admin/brand/add")
	public String addBrand(Model model) {
		model = populateModelForAdd(model, new Brand());
		return "admin-pages/brand/add";
	}

	@RequestMapping(value = "/admin/brand/add", method = RequestMethod.POST)
	public String brandAdd(Brand brand, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		brandValidator.validate(brand, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, brand);
			return "admin-pages/brand/add";
		}
		brandService.createBrand(brand, loggedInUser, true,false);
		String message = "Brand added successfully";
		return "redirect:/admin/brand/list?message=" + message;
	}

	@RequestMapping(value = "/admin/brand/list")
	public String getBrandList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<Brand> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = brandService.searchBrand(pageNumber, searchTerm, user,true,false);
		} else {
			page = brandService.getBrands(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("brands", page.getContent());

		return "/admin-pages/brand/list";
	}

	@RequestMapping(value = "/admin/brand/show/{cipher}")
	public String showBrand(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Brand brand = brandService.getBrand(id, true, loggedIUser,true,false);
		model.addAttribute("brand", brand);
		return "/admin-pages/brand/show";
	}

	@RequestMapping(value = "/admin/brand/update/{cipher}")
	public String updateBrand(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Brand brand = brandService.getBrand(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, brand);
		model.addAttribute("id", brand.getId());
		model.addAttribute("version", brand.getVersion());
		return "/admin-pages/brand/update";
	}

	@RequestMapping(value = "/admin/brand/update", method = RequestMethod.POST)
	public String brandUpdate(Brand brand, 
										BindingResult formBinding, 
										Model model) {

		brand.setUpdateOperation(true);
		brand.setId(Long.parseLong(model.asMap().get("id") + ""));
		brand.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		brandValidator.validate(brand, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, brand);
			return "admin-pages/brand/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		brandService.updateBrand(brand.getId(), brand, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Brand updated successfully";
		return "redirect:/admin/brand/list?message=" + message;
	}

	@RequestMapping(value = "/admin/brand/delete/{cipher}")
	public String deleteBrand(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			brandService.deleteBrand(id);
			message = "Brand deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Brand";
		}
		return "redirect:/admin/brand/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
}
