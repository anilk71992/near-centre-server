package com.vishwakarma.nearcentre.messaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MessageContent;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;

@Component
public class EmailHelper {

	@Resource
	private JavaMailSenderImpl javaMailSenderImpl;
	
	@Resource
	private Environment environment;

	@Resource
	private VelocityEngine velocityEngine;
	
	private boolean isEnabled;
	
	private MandrillApi mandrilApi;
	
	@PostConstruct
	public void doInit() {
		isEnabled = Boolean.parseBoolean(environment.getProperty("email.enabled", "false"));
		mandrilApi = new MandrillApi(environment.getProperty("email.password", ""));
	}

	private void sendMail(SimpleMailMessage message) {
		if(isEnabled)
			javaMailSenderImpl.send(message);
	}
	
	private void sendMail(MimeMessagePreparator messagePreparator) {
		//if(isEnabled)
			javaMailSenderImpl.send(messagePreparator);
	}
	
	@Async
	public void sendSimpleEmail(String email, String subject, String messageBody) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom(environment.getProperty("email.senderName"));
		simpleMailMessage.setTo(email);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(messageBody);
		simpleMailMessage.setReplyTo(environment.getProperty("email.replyTo"));
		sendMail(simpleMailMessage);
	}
	
	@Async
	public void sendMailForPasswordRecovery(final String email,final String subject,final String messageBody) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(email);
				mimeMessageHelper.setSubject(subject);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public boolean sendMailWithMandrilTemplate(List<String> emailIds, String subject, String templateName, InputStream picAttachment) {
		
		if(!isEnabled)
			return true;
		
		MandrillMessage mandrillMessage = new MandrillMessage();
		mandrillMessage.setSubject(subject);
		mandrillMessage.setFromEmail(environment.getProperty("email.senderEmail"));
		mandrillMessage.setFromName(environment.getProperty("email.senderName"));
		
		List<Recipient> recipients = new ArrayList<MandrillMessage.Recipient>();
		for (String emailId : emailIds) {
			Recipient recipient = new Recipient();
			recipient.setEmail(emailId);
			recipients.add(recipient);
		}
		mandrillMessage.setTo(recipients);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Reply-To", environment.getProperty("email.replyTo"));
		mandrillMessage.setHeaders(headers);
		
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("website", "www.nearCentre.com");
		mandrillMessage.setMetadata(metadata);
		
		if(null != picAttachment) {

			List<MessageContent> attachments = new ArrayList<MandrillMessage.MessageContent>();
			MessageContent content = new MessageContent();
			content.setName("attachment.jpg");
			content.setType("image/jpeg");
			try {
				content.setContent(Base64.getEncoder().encodeToString(IOUtils.toByteArray(picAttachment)));
				attachments.add(content);
				mandrillMessage.setAttachments(attachments);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			mandrilApi.messages().sendTemplate(templateName, new HashMap<String, String>(), mandrillMessage, true);
			return true;
		} catch (MandrillApiError e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
