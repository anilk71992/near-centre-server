package com.vishwakarma.nearcentre.property.editor;

import java.beans.PropertyEditorSupport;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.nearcentre.model.ServiceCentre;
import com.vishwakarma.nearcentre.repository.ServiceCentreRepository;

public class ServiceCentrePropertyEditor extends PropertyEditorSupport{
	
	private ServiceCentreRepository serviceCentreRepository;
	
	private Logger log=LoggerFactory.getLogger(ServiceCentrePropertyEditor.class);

	public ServiceCentrePropertyEditor(ServiceCentreRepository serviceCentreRepository)
	{
		this.serviceCentreRepository=serviceCentreRepository;
	}
	
	@Override
	public String getAsText()
	{
		ServiceCentre obj=(ServiceCentre)getValue();
		
		if(obj==null)
		{
			return "";
		}
		return obj.toString();
		
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException
	{
		try
		{
			Long id=Long.parseLong(text);
			if(id<0)
			{
				super.setValue(null);
				return;
			}
			
		
			ServiceCentre serviceCentre=serviceCentreRepository.findOne(id);
			if(serviceCentre!=null)
			{
				super.setValue(serviceCentre);
			}
			else
			{
				Log.error("Binding Error:Can not find City with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find City with id - " + text);
			}
		}
		catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
		}
	}
	

}
