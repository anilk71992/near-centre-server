package com.vishwakarma.nearcentre.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.repository.BrandRepository;


public class BrandPropertyEditor extends PropertyEditorSupport{

	private BrandRepository brandRepository;
	
	private Logger log = LoggerFactory.getLogger(BrandPropertyEditor.class);
	
	public BrandPropertyEditor(BrandRepository brandRepository) {
		this.brandRepository = brandRepository;
	}
	
	@Override
	public String getAsText() {
		Brand obj = (Brand) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Brand brand = brandRepository.findOne(id);
			
			if(null != brand) {

				super.setValue(brand);
			} else {
				log.error("Binding Error:Can not find Brand with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Brand with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
		}
	}

	
}
