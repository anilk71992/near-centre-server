package com.vishwakarma.nearcentre.property.editor;

import java.beans.PropertyEditorSupport;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.nearcentre.model.Product;
import com.vishwakarma.nearcentre.repository.ProductRepository;

public class ProductPropertyEditor extends PropertyEditorSupport{
	
	private ProductRepository productRepository;
	
	private Logger log=LoggerFactory.getLogger(ProductPropertyEditor.class);

	public ProductPropertyEditor(ProductRepository productRepository)
	{
		this.productRepository=productRepository;
	}
	
	@Override
	public String getAsText()
	{
		Product obj=(Product)getValue();
		
		if(obj==null)
		{
			return "";
		}
		return obj.toString();
		
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException
	{
		try
		{
			Long id=Long.parseLong(text);
			if(id<0)
			{
				super.setValue(null);
				return;
			}
			Product product=productRepository.findOne(id);
			if(product!=null)
			{
				super.setValue(product);
			}
			else
			{
				Log.error("Binding Error:Can not find City with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find City with id - " + text);
			}
		}
		catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
		}
	}
	

}
