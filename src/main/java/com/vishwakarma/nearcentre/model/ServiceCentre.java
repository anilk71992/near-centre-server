package com.vishwakarma.nearcentre.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


@Entity(name= "ServiceCentre")
@Table(name="ServiceCentre", uniqueConstraints= {@UniqueConstraint (columnNames= {"code"})})
public class ServiceCentre extends BaseEntity{
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String serviceCentreName;

	@NotNull
	@NotEmpty
	private String serviceCentreAddress;
	
	@ManyToOne
	@JoinColumn(name="cityId",nullable=false,referencedColumnName="id")
	private City city;
	
	@NotNull
	@NotEmpty
	private String serviceCentrePostalCode;
	
	@NotNull
	private Double serviceCentreLatitude;
	
	@NotNull
	private Double serviceCentreLongitude;
	
	@NotNull
	@NotEmpty
	private String serviceCentreStartTime;

	@NotNull
	@NotEmpty
	private String serviceCentreEndTime;
	
	@PostPersist
	public void populateCode(){
		setCode("SC-"+getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getServiceCentreName() {
		return serviceCentreName;
	}

	public void setServiceCentreName(String serviceCentreName) {
		this.serviceCentreName = serviceCentreName;
	}

	
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getServiceCentreAddress() {
		return serviceCentreAddress;
	}

	public void setServiceCentreAddress(String serviceCentreAddress) {
		this.serviceCentreAddress = serviceCentreAddress;
	}


	public String getServiceCentrePostalCode() {
		return serviceCentrePostalCode;
	}

	public void setServiceCentrePostalCode(String serviceCentrePostalCode) {
		this.serviceCentrePostalCode = serviceCentrePostalCode;
	}

	public Double getServiceCentreLatitude() {
		return serviceCentreLatitude;
	}

	public void setServiceCentreLatitude(Double serviceCentreLatitude) {
		this.serviceCentreLatitude = serviceCentreLatitude;
	}

	public Double getServiceCentreLongitude() {
		return serviceCentreLongitude;
	}

	public void setServiceCentreLongitude(Double serviceCentreLongitude) {
		this.serviceCentreLongitude = serviceCentreLongitude;
	}

	public String getServiceCentreStartTime() {
		return serviceCentreStartTime;
	}

	public void setServiceCentreStartTime(String serviceCentreStartTime) {
		this.serviceCentreStartTime = serviceCentreStartTime;
	}

	public String getServiceCentreEndTime() {
		return serviceCentreEndTime;
	}

	public void setServiceCentreEndTime(String serviceCentreEndTime) {
		this.serviceCentreEndTime = serviceCentreEndTime;
	}


	
	



}
