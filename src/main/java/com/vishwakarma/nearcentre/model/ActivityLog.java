/**
 * 
 */
package com.vishwakarma.nearcentre.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="ActivityLog", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class ActivityLog extends BaseEntity {

	private String code;
	
	@NotNull
	@NotEmpty
	private String activity,entityType;
	
	private String changeStatus;
	
	private String miscellaneous;
	
	private Long entityId;
	
	@ManyToOne
	@JoinColumn(name="userId", nullable=true, referencedColumnName="id")
	private User user;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@PostPersist
	public void populateCode() {
		setCode("AL-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getChangeStatus() {
		return changeStatus;
	}

	public void setChangeStatus(String changeStatus) {
		this.changeStatus = changeStatus;
	}
	
	
}
