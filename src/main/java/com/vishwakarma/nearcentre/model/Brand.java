package com.vishwakarma.nearcentre.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity(name="Brand")
@Table(name="Brand",uniqueConstraints= {@UniqueConstraint (columnNames = { "code" })})
public class Brand extends BaseEntity{
	
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String brandName;
	
	/*@ManyToMany(cascade = CascadeType.ALL, mappedBy)
	//@JoinTable(name="BrandProduct", joinColumns= @JoinColumn(name= "brandId"), inverseJoinColumns= @JoinColumn(name="productId"))
	private Set<Product> products=new HashSet<Product>();
	
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}*/

	@NotNull
	@NotEmpty
	private String brandColor;
	
	@PostPersist
	public void populateCode(){
		setCode("B-"+getId());
	}
	
	/*@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="BrandProductSCentre",joinColumns=@JoinColumn(name="brandCode",referencedColumnName="code"),inverseJoinColumns= @JoinColumn(name="productCode",referencedColumnName="code"))
	private Set<Product> product=new HashSet<Product>();
	
	private Set<ServiceCentre> serviceCentre;

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="BrandProductSCentre",joinColumns=@JoinColumn(name="brandCode",referencedColumnName="code"),inverseJoinColumns= @JoinColumn(name="serviceCentreCode",referencedColumnName="code"))
	public Set<ServiceCentre> getServiceCentre() {
		return serviceCentre;
	}

	public void setServiceCentre(Set<ServiceCentre> serviceCentre) {
		this.serviceCentre = serviceCentre;
	}*/

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandColor() {
		return brandColor;
	}

	public void setBrandColor(String brandColor) {
		this.brandColor = brandColor;
	}


	/*public Set<Product> getProduct() {
		return product;
	}

	public void setProduct(Set<Product> product) {
		this.product = product;
	}*/
	
	
	
	

}
