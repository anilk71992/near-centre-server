package com.vishwakarma.nearcentre.model;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.nearcentre.utils.CommonUtils;

/**
 * @author Vishal
 *
 */
@Entity(name="User")
@Table(name="User", uniqueConstraints={
		@UniqueConstraint(columnNames = {"code", "mobileNumber", "email"})})
@Inheritance(strategy=InheritanceType.JOINED)
public class User extends BaseEntity{
	
	public static enum ReferredMedia{
		Social_Media,
		Website,
		Newspaper,
		Radio,
		Event,
		Friend
	}

	private String code;
	
	@NotNull
	@NotEmpty
	private String firstName;
	
	private String lastName;
	
	@Email
	@Column(nullable=true)
	private String email;
	
	@NotNull
	@NotEmpty
	private String mobileNumber;
	
	private String alterNateMobileNumber;
	
	@Column(nullable=true)
	private String password;
	
	@Column(unique=true,nullable=false,updatable=false)
	private String username;

	private String fbId, gPlusId, twitterId;
	
	@Enumerated(EnumType.STRING)
	private ReferredMedia referredMedia;

	private Double lat, lng;
	
	@ManyToOne
	@JoinColumn(name="referredBy", nullable=true, referencedColumnName="id")
	private User referredBy;
	
	private String referralCode;
	
	@Column(columnDefinition="int(11) default '0'")
	private int numberOfPosts, numberOfExperReviews, numberOfComments, numberOfSolutions;
	
	@Column(columnDefinition="tinyint(1) default NULL")
	private Boolean gender;

	private Date dob, joiningDate;

	private String profilePicUrl;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "UserRole", joinColumns =@JoinColumn(name = "userId"), inverseJoinColumns =@JoinColumn(name = "roleId"))
	private Set<Role> userRoles=new HashSet<Role>();	

	@NotNull
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean selecetedContributionWays;
	
	@NotNull
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isEnabled;
	
	@NotNull
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isVerified;
	
	private String verificationCode;
	
	@Transient
	private byte[] profilePicData;
	
	@Transient
	private String fileName;
	
	@Transient
	private Logger logger = LoggerFactory.getLogger(User.class);
	
	@PrePersist
	public void setDefaults() {
		if (StringUtils.isEmpty(username)) {
			username = mobileNumber;
		}
	}
	
	@PostPersist
	public void onPostPersist() {
		setCode("U-" + getId());
		createProfilePicFile();
		if(StringUtils.isBlank(getReferralCode()))
			setReferralCode(CommonUtils.getReferralCodeForUser(this));
	}
	
	public void createProfilePicFile() {
		if(null != profilePicData) {
			try {
				File profilePic = getProfilePicFile();
				FileUtils.writeByteArrayToFile(profilePic, profilePicData);
				setProfilePicUrl(profilePic.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlterNateMobileNumber() {
		return alterNateMobileNumber;
	}

	public void setAlterNateMobileNumber(String alterNateMobileNumber) {
		this.alterNateMobileNumber = alterNateMobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getgPlusId() {
		return gPlusId;
	}

	public void setgPlusId(String gPlusId) {
		this.gPlusId = gPlusId;
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}
	
	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public Set<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<Role> userRoles) {
		this.userRoles = userRoles;
	}
	
	public boolean getSelecetedContributionWays() {
		return selecetedContributionWays;
	}

	public void setSelecetedContributionWays(boolean selecetedContributionWays) {
		this.selecetedContributionWays = selecetedContributionWays;
	}

	public boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public byte[] getProfilePicData() {
		return profilePicData;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getReferredBy() {
		return referredBy;
	}
	
	public void setReferredBy(User referredBy) {
		this.referredBy = referredBy;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public ReferredMedia getReferredMedia() {
		return referredMedia;
	}

	public void setReferredMedia(ReferredMedia referredMedia) {
		this.referredMedia = referredMedia;
	}

	/**
	 * Don't forget to call {@linkplain setFileName()}
	 * @param profilePicData
	 */
	public void setProfilePicData(byte[] profilePicData) {
		this.profilePicData = profilePicData;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setUserRole(Role role) {
		this.userRoles = new HashSet<Role>();
		this.userRoles.add(role);
	}
	
	public boolean hasRole(String roleName) {
		for (Role role : userRoles) {
			if (role.getRole().equals(roleName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns whether user has any role specified in CSV string
	 * @param roleCSV - Comma Separated Role Names
	 * @return
	 */
	public boolean hasAnyRole(String roleCSV) {
		String[] rolesArray = roleCSV.split(",");
		for (int i = 0; i < rolesArray.length; i++) {
			rolesArray[i] = rolesArray[i].trim();
		}
		List<String> roles = Arrays.asList(rolesArray);
		for (Role role : userRoles) {
			if(roles.contains(role.getRole())) {
				return true;
			}
		}
		return false;
	}
	
	public void addRole(Role role) {
		if(this.userRoles == null) {
			this.userRoles = new HashSet<Role>();
		}
		this.userRoles.add(role);
	}
	
	public void removeRole(Role role) {
		if(this.userRoles != null) {
			this.userRoles.remove(role);
		}
	}

	public String getFullName() {
		return firstName+" "+lastName;
	}
	
	@Override
	public String toString() {
		return firstName+" "+lastName;
	}
	
	private File getProfilePicFile() {
		
		File baseDir = new File(System.getProperty("catalina.base"), "userProfilePics");
		if(!baseDir.exists() || !baseDir.isDirectory()) {
			baseDir.mkdir();
		}
		File profilePic = new File(baseDir, this.getId() + "_" + this.getFileName());
		if(profilePic.exists() && !profilePic.isDirectory()) {
			profilePic.delete();
		}
		return profilePic;
	}

}
