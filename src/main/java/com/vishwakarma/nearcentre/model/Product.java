package com.vishwakarma.nearcentre.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


@Entity(name= "Product")
@Table(name="Product", uniqueConstraints= {@UniqueConstraint (columnNames= {"code"})})
public class Product extends BaseEntity{
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String productName;
	

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="BrandProduct", joinColumns= @JoinColumn(name= "brandId"), inverseJoinColumns= @JoinColumn(name="productId"))
	private Set<Brand>  brands=new HashSet<Brand>();
	//private Set<ServiceCentre> serviceCentre;
	
	

	@PostPersist
	public void populateCode(){
		setCode("P-"+getId());
		
		
	}

	public Set<Brand> getBrand() {
		return brands;
	}

	public void setBrand(Set<Brand> brand) {
		this.brands = brands;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	
	/*@ManyToMany(mappedBy="BrandProductSCentre")
	public Set<ServiceCentre> getServiceCentre() {
		return serviceCentre;
	}

	public void setServiceCentre(Set<ServiceCentre> serviceCentre) {
		this.serviceCentre = serviceCentre;
	}*/

}
