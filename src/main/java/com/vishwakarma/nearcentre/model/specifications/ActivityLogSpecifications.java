package com.vishwakarma.nearcentre.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.nearcentre.utils.DateHelper;

public class ActivityLogSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("code"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("activity"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("entityType"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("firstName"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("lastName"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("mobileNumber"), "%"+searchTerm+"%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("email"), "%"+searchTerm+"%")));

				return predicate;
			}

		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByDateRangeAndChangeStatus(final String status, final Date fromDate, final Date toDate){
				return new Specification() {
		
					@SuppressWarnings("unchecked")
					@Override
					public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

						return builder.and(getPredicateByChangeStatus(status, item, builder), getPredicateByDateRange(fromDate, toDate, item, builder));
					}
				};
			}

	
	@SuppressWarnings("unchecked")
	public static Predicate getPredicateByChangeStatus(String status, Root root, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.or(builder.like(root.get("changeStatus"), "%" + status + "%")));
		
		return predicate;
	
	}
	
	
	@SuppressWarnings("unchecked")
	public static Predicate getPredicateByDateRange(Date fromDate, Date toDate, Root item, CriteriaBuilder builder) {
		Predicate predicate = builder.and();
		
		Date startOfTheDay = new Date(), endOfTheDay = new Date();
		
		startOfTheDay.setTime(fromDate.getTime());
		endOfTheDay.setTime(toDate.getTime());
		
		DateHelper.setToStartOfDay(startOfTheDay);
		DateHelper.setToEndOfDay(endOfTheDay);
		
		predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
        predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
		return predicate;
	
	}
	
	
	public static Specification getActivityByEntityIdAndStatus(final Long entityId, final String status) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.or(builder.equal(item.get("entityId"),entityId)));
				predicate.getExpressions().add(builder.or(builder.notLike(item.get("changeStatus"),status)));
				
				return predicate;
			}

		};
	}
}
