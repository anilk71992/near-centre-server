package com.vishwakarma.nearcentre.model.specifications;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class UserSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or(getPredicateBySearchTerm(searchTerm, item, builder));
				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchBySearchTermAndRole(final String searchTerm, final String role) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

				return builder.and(getPredicateBySearchTerm(searchTerm, item, builder), getPredicateForRole(role, item, builder));
			}

		};
	}

	@SuppressWarnings("unchecked")
	private static Predicate getPredicateBySearchTerm(String searchTerm, Root root, CriteriaBuilder builder) {

		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.or(builder.like(root.get("code"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("firstName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("lastName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("email"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("username"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("mobileNumber"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("alterNateMobileNumber"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("referralCode"), "%" + searchTerm + "%")));
		return predicate;
	}
	
	@SuppressWarnings("rawtypes")
	private static Predicate getPredicateForRole(String role, Root root, CriteriaBuilder cb) {

		Predicate predicate = cb.and();
		Join join = root.joinSet("userRoles");
		predicate.getExpressions().add((join.get("role").in(role)));
		return predicate;
	}
}
