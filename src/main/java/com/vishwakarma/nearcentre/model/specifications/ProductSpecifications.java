package com.vishwakarma.nearcentre.model.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class ProductSpecifications {
	public static Specification search(final String searchTerm)
	{
		return new Specification() {
			public Predicate toPredicate(Root brand,CriteriaQuery query, CriteriaBuilder builder)
			{
				Predicate predicate=builder.or();
				predicate.getExpressions().add(builder.or(builder.like(brand.get("productName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(brand.get("code"), "%" + searchTerm + "%")));
				return predicate;
			}
		};
	}
}
