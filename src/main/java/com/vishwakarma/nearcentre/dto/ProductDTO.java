package com.vishwakarma.nearcentre.dto;

import com.vishwakarma.nearcentre.model.Product;

public class ProductDTO {
	
	Long id;
	private String code,name,color;
	
	public ProductDTO()
	{
		
	}
	
	public ProductDTO(Product  product)
	{
		setId(product.getId());
		setCode(product.getCode());
		setName(product.getProductName());
	
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
