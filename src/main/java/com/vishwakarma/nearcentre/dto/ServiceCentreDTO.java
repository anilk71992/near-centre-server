package com.vishwakarma.nearcentre.dto;

import com.vishwakarma.nearcentre.model.ServiceCentre;

public class ServiceCentreDTO {
	
	Long id;
	private String code,name,address,postalCode,startTime,endTime;
	private Double latitude,longitude;
	
	public ServiceCentreDTO()
	{
		
	}
	
	public ServiceCentreDTO(ServiceCentre  serviceCentre)
	{
		setId(serviceCentre.getId());
		setCode(serviceCentre.getCode());
		setName(serviceCentre.getServiceCentreName());
		setAddress(serviceCentre.getServiceCentreAddress());
		setPostalCode(serviceCentre.getServiceCentrePostalCode());
		setLatitude(serviceCentre.getServiceCentreLatitude());
		setLongitude(serviceCentre.getServiceCentreLongitude());
	
		
	}



	private void setLatitude(Double serviceCentreLatitude) {
		this.latitude=latitude;
	}

	private void setLongitude(Double serviceCentreLongitude) {
		this.longitude=longitude;
		
	}

	public void setPostalCode(String serviceCentrePostalCode) {
		this.postalCode=postalCode;
		
	}

	public void setAddress(String serviceCentreAddress) {
		this.address=address;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
