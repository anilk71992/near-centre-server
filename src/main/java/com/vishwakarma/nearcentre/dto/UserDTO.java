package com.vishwakarma.nearcentre.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.utils.DateHelper;

/**
 * @author Vishal
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserDTO implements Serializable {

	private long id;

	private String referredMedia;

	private String firstName, lastName, email, mobileNumber, username, profilePicUrl, dob, fbId, gPlusId, twitterId, password, verificationCode, alterNateMobileNumber;

	private String code, referredBy, referralCode;

	private double lat, lng;

	private boolean gender, isEnabled;

	private List<String> userRoles = new ArrayList<String>();

	private boolean isVerified = false;

	public UserDTO() {

	}

	public UserDTO(User user) {
		setId(user.getId());
		setCode(user.getCode());
		setFirstName(user.getFirstName());
		setLastName(user.getLastName());
		setEmail(user.getEmail());
		setMobileNumber(user.getMobileNumber());
		setUsername(user.getUsername());
		setPassword(user.getPassword());
		setProfilePicUrl("/restapi/getProfilePicture?email=" + user.getEmail());
		
		if (null != user.getReferredMedia())
			setReferredMedia(user.getReferredMedia().name());
		if (null != user.getReferredBy())
			setReferredBy(user.getReferredBy().getCode());
		
		setReferralCode(user.getReferralCode());
		setAlterNateMobileNumber(user.getAlterNateMobileNumber());
		
		if (null != user.getLat())
			setLat(user.getLat());

		if (null != user.getLng())
			setLng(user.getLng());

		if (null != user.getDob())
			setDob(DateHelper.getFormattedDate(user.getDob()));
		setGender(user.getGender());
		setIsEnabled(user.getIsEnabled());
		setIsVerified(user.getIsVerified());
		for (Role role : user.getUserRoles()) {
			userRoles.add(role.getRole());
		}
		setFbId(user.getFbId());
		setgPlusId(user.getgPlusId());
		setTwitterId(user.getTwitterId());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReferredMedia() {
		return referredMedia;
	}

	public void setReferredMedia(String referredMedia) {
		this.referredMedia = referredMedia;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getgPlusId() {
		return gPlusId;
	}

	public void setgPlusId(String gPlusId) {
		this.gPlusId = gPlusId;
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public List<String> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<String> userRoles) {
		this.userRoles = userRoles;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getAlterNateMobileNumber() {
		return alterNateMobileNumber;
	}

	public void setAlterNateMobileNumber(String alterNateMobileNumber) {
		this.alterNateMobileNumber = alterNateMobileNumber;
	}
	
}
