package com.vishwakarma.nearcentre.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.nearcentre.model.Brand;


@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class BrandDTO implements Serializable{

	
	Long id;
	private String code,name,color;
	
	public BrandDTO()
	{
		
	}
	
	public BrandDTO(Brand brand)
	{
		setId(brand.getId());
		setCode(brand.getCode());
		setName(brand.getBrandName());
		setColor(brand.getBrandColor());
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	

	
	
}
