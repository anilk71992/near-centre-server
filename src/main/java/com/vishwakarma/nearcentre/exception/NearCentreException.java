package com.vishwakarma.nearcentre.exception;

public class NearCentreException extends RuntimeException{
		 
		private String exceptionMsg;
		private String errorCode = "400";
	 
		public NearCentreException() {
			super();
		}
		
		public NearCentreException(String exceptionMsg, String errorCode) {
			super();
			this.exceptionMsg = exceptionMsg;
			this.errorCode = errorCode;
		}

		public NearCentreException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}

		public NearCentreException(Throwable arg0) {
			super(arg0);
		}

		public NearCentreException(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
		
		public String getExceptionMsg(){
			return this.exceptionMsg;
		}
		
		public void setExceptionMsg(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
}

