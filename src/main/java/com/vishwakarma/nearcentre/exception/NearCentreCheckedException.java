package com.vishwakarma.nearcentre.exception;

public class NearCentreCheckedException extends Exception{
		 
		protected String exceptionMsg;
	 
		public NearCentreCheckedException() {
			super();
		}
				
		public NearCentreCheckedException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}


		public NearCentreCheckedException(Throwable arg0) {
			super(arg0);
		}

		public NearCentreCheckedException(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
		
		public String getExceptionMsg(){
			return this.exceptionMsg;
		}
		
		public void setExceptionMsg(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
}

