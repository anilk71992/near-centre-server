package com.vishwakarma.nearcentre.utils;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.vishwakarma.nearcentre.messaging.EmailHelper;
import com.vishwakarma.nearcentre.service.CityService;
import com.vishwakarma.nearcentre.service.UserService;

@Component
public class ScheduledJobs {
	
	private final static Logger log = LoggerFactory.getLogger(ScheduledJobs.class);

	@Resource 
	private  UserService userService;
	
	@Resource
	private NotificationHelper  notificationHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private CityService cityService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private AsyncJobs asyncJobs;

}
