package com.vishwakarma.nearcentre.utils;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.vishwakarma.nearcentre.messaging.EmailHelper;
import com.vishwakarma.nearcentre.model.User;



@Component
public class NotificationHelper {
	
	@Resource 
	private EmailHelper emailHelper;
	
	public void sendNotificationForNewAccount(User user, String password) {
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailHelper.sendSimpleEmail(user.getEmail(), "Welcome to nearCentre", getEmailBodyForNewAccount(user, password));
		}
	}	
	
	public String getSmsBodyForNewAccount(User user, String password) {
		String smsText = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			smsText = "Your account is successfully created on nearCentre. Your Username is " +  user.getEmail() +
					"and Password is " + password;
		}
		return smsText;
	}
	
	public String getSmsBodyForPasswordReset(User user, String password) {
		String smsText = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			smsText = "Your password is successfully changed for nearCentre Account. Your Username is " +  user.getEmail() +
					"and new Password is " + password;
		}
		return smsText;
	}
	
	public String getEmailBodyForNewAccount(User user, String password) {
		String emailBody = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailBody = "Hello " + user.getFullName() + ",\n\n" + 
					"Thanks for signing up with nearCentre. NearCentre're really excited to have you onboard!." + "\n\n" + 
					"Your account details are as follows:-" + "\n\n" + 
					"Username- " + user.getEmail() + "\n" +
					"Password- " + password + "\n\n" +
					"You can use nearCentre as an Individual user to connect with businesses"+
					"worldwide as well as use nearCentre as a business user to connect with your"+
					"existing and new customers. You will see that nearCentre is an extremely easy to use and very useful Mobile App."+"\n\n"+
					"As an individual user you can seek customer support from various businesses"+
					"easily, find and order products and services from various small and large"+
					"businesses, order food from Restaurants, find doctors, florists, handymen,"+
					"architects and millions of other small and local businesses."+"\n\n"+
					"As a business user, your existing customers will be able to chat with you"+
					"for customer service and new customers will be able to find you to order"+
					"your products and services. You will be able to create happier customers as"+
					"well as get many new customers. nearCentre will help your Business grow."+"\n\n"+
					"Best Regards,"+"\n"+
					"Team nearCentre";
		}
		return emailBody;
	}
	
	public String getEmailBodyForPasswordReset(User user, String password) {
		String emailBody = "No Email Found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailBody = "Hi " + user.getFullName() + ",\n" + 
					"Your password has been successfully changed for your nearCentre account." + "\n" + 
					"Your new account credentials are as follows-" + "\n" +
					"Username- " + user.getEmail() + "\n" +
					"Password- " + password;
		}
		return emailBody;
	}
}
