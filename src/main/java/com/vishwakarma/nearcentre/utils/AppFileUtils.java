package com.vishwakarma.nearcentre.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * @author Vishal
 *
 */
@Component
public class AppFileUtils {

	@Resource
	private Environment environment;
	
	private AmazonS3Client amazonS3Client;
	
	private boolean isAwsEnabled;
	
	private String bucket;
	
	@PostConstruct
	protected void initClient() {
		isAwsEnabled = Boolean.parseBoolean(environment.getProperty("aws.s3.enabled", "false"));
		if(isAwsEnabled) {
			amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(environment.getProperty("aws.access.keyid"), environment.getProperty("aws.access.keysecret")));
		}
		bucket = environment.getProperty("aws.s3.bucket");
	}

	/**
	 * @param folder - Path after catalina.base e.g. profilePics, servicePics
	 * @param file - Only the name of file usually entityId 
	 */
	@Async
	public void saveData(String folder, String fileName, byte[] data) {

		if(isAwsEnabled) {
			saveOnS3(folder, fileName, data);
		} else {
			saveLocally(folder, fileName, data);
		}
	}
	
	public String getS3Url(String folder, String fileName) {
		return amazonS3Client.getResourceUrl(bucket, folder + "/" + fileName);
	}
	
	private void saveLocally(String folder, String fileName, byte[] data) {
		File baseDir = new File(System.getProperty("catalina.base"), folder);
		if(!baseDir.exists() || !baseDir.isDirectory()) {
			baseDir.mkdir();
		}
		File file = new File(baseDir, fileName);
		if(file.exists() && !file.isDirectory()) {
			file.delete();
		}
		try {
			FileUtils.writeByteArrayToFile(file, data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void saveOnS3(String folder, String fileName, byte[] data) {
		amazonS3Client.putObject(bucket, folder + "/" + fileName, new ByteArrayInputStream(data), null);
	}
	
}
