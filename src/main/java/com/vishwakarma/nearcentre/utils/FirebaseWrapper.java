package com.vishwakarma.nearcentre.utils;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirebaseWrapper {

	private static enum NotificationType {
	}

	private static String FIREBASE_URL = "";
	private static String APP_ID = "";
	private static String REST_API_KEY = "";
	private static String PARSE_URL = "https://api.parse.com/1/push"; 
	private static Logger logger = LoggerFactory.getLogger(FirebaseWrapper.class);

	public static void sendSimpleMessage(String message) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("message", message);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		sendFirebaseRequest(jsonObject, "U-1", "U-2");
	}
	
	private static void sendFirebaseRequest(JSONObject dataObject, String... channels) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("data", dataObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		for(int i = 0; i < channels.length; i++) {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(FIREBASE_URL + "/nearCentre/users/" + channels[i] + ".json");
	
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
	
			StringEntity stringEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
			httpPost.setEntity(stringEntity);
	
			try {
				HttpResponse httpResponse = httpClient.execute(httpPost);
				logger.debug("================== Firebase Response Code ================== " + httpResponse.getStatusLine().getStatusCode());
				String response = IOUtils.toString(httpResponse.getEntity().getContent());
				logger.debug("================== Firebase Response ================== " + response);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void sendParseRequest(JSONObject dataObject, String... channels) {
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("data", dataObject);
			JSONArray channelArray = new JSONArray();
			for (int i = 0; i < channels.length; i++) {
				channelArray.put(channels[i]);
			}
			jsonObject.put("channels", channelArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(PARSE_URL);
		
		httpPost.setHeader("X-Parse-Application-Id", APP_ID);
		httpPost.setHeader("X-Parse-REST-API-Key", REST_API_KEY);
		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		
		StringEntity stringEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
		httpPost.setEntity(stringEntity);
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpPost);
			logger.debug("================== Parse Response Code ================== " + httpResponse.getStatusLine().getStatusCode());
			String response = IOUtils.toString(httpResponse.getEntity().getContent());
			logger.debug("================== Parse Response ================== " + response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
