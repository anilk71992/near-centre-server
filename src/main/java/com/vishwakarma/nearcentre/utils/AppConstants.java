package com.vishwakarma.nearcentre.utils;

public class AppConstants {

	public static final int PAGE_SIZE = 10;
	public static final int PASSWORD_LENGTH = 6;
	public static final String IMPORT_EXCEL_FORMAT = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String CLONED_SUBSCRIPTION_NOTE = "SUBSCRIPTION_CLONED";
	public static final String PROVILAC_IMAGES_FOLDER = "weImages";
	public static final String PRODUCT_PICS_FOLDER = "productPics";
	public static final String NOTIFICATION_IMAGES_FOLDER ="notificationImages";
	public static final String PRODUCT_BANNERS_FOLDER = "productBanners";
	
	//starting from sunday 
	public static enum Day {
		Day_1,
		Day_2,
		Day_3,
		Day_4,
		Day_5,
		Day_6,
		Day_7
	}
}
