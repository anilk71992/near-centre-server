package com.vishwakarma.nearcentre.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.Product;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.model.specifications.ProductSpecifications;
import com.vishwakarma.nearcentre.repository.ProductRepository;
import com.vishwakarma.nearcentre.utils.AppConstants;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;


@Service
public class ProductService {

	@Resource
	private ProductRepository productRepository;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public Product createProduct(Product product, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == product) {
			throw new NearCentreException("Invalid product");
		}
		product = productRepository.save(product);
		activityLogService.createActivityLog(loggedInUser, "add", "Product", product.getId());
		if (populateTransientFields) {
			product = populateTransientFields(product, loggedInUser);
		}

		if(fetchEagerly) {
			product= fetchEagerly(product);
		}
		return product;
	}
	
	@Transactional
	public List<Product> getAllProducts(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<Product> products = productRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (Product product : products) {
				if(populateTransientFields)
					product = populateTransientFields(product, loggedInUser);
				if (fetchEagerly)
					product = fetchEagerly(product);
			}
		}
		return products;
	}
	
	@Transactional
	public Page<Product> getProducts(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Product> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = productRepository.findAll(pageRequest);
		Iterator<Product> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Product product = (Product) iterator.next();
				if(populateTransientFields)
					product = populateTransientFields(product, loggedInUser);
				if(fetchEagerly)
					product = fetchEagerly(product);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Product> searchProduct(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Product> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = productRepository.findAll(ProductSpecifications.search(searchTerm), pageRequest);
		Iterator<Product> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Product product = (Product) iterator.next();
				if(populateTransientFields)
					product = populateTransientFields(product, loggedInUser);
				if(fetchEagerly)
					product = fetchEagerly(product);
			}
		}
		return page;
	}
	
	public Product getProduct(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid product id");
		}
		Product product = productRepository.findOne(id);
		if(doPostValidation && null == product) {
			throw new NearCentreException("Product with this id does not exists");
		}
		if(null == product){
			return null;
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);
		
		return product;
	}
	
	public Product getProductByName(String productName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(productName)) {
			throw new NearCentreException("Invalid product name");
		}
		Product product = productRepository.findByProductName(productName);
		if(doPostValidation && null == product) {
			throw new NearCentreException("Product with this name does not exists");
		}
		if(product == null){
			return null;
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);
		
		return product;
	}
	
	/**
	 * 
	 * Updates the Product.
	 * @param id - Id of existing Product
	 * @param Product - Updated Product
	 * @return Updated Product
	 */
	public Product updateProduct(Long id, Product product, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid Product id");
		}
		Product existingProduct = productRepository.findOne(id);
		
		if(null == existingProduct) {
			throw new NearCentreException("Product with this id does not exists");
		}
		existingProduct.setProductName(product.getProductName());
		
		existingProduct = productRepository.save(existingProduct);
		activityLogService.createActivityLog(loggedInUser, "update", "Product",id);
		existingProduct.setCipher(EncryptionUtil.encode(product.getId()+""));
		if(populateTransientFields)
			existingProduct =populateTransientFields(existingProduct, loggedInUser);
		if(fetchEagerly)
			existingProduct=fetchEagerly(existingProduct);
		return existingProduct;
	}
	
	public void deleteProduct(Long id) {
		
		if(null == id) {
			throw new NearCentreException("Invalid product id");
		}
		Product product = productRepository.findOne(id);
		if(null == product) {
			throw new NearCentreException("Product with this id does not exists");
		}
		productRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Product",id);
	}
	
	private Product populateTransientFields(Product product, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new NearCentreException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			product.setIsEditAllowed(true);
			product.setIsDeleteAllowed(true);
		} else {
			product.setIsEditAllowed(false);
			product.setIsDeleteAllowed(false);
		}
		product.setCipher(EncryptionUtil.encode(product.getId()+""));
		return product;
	}
	
	private Product fetchEagerly(Product product) {
		
		return product;
	}

	
}