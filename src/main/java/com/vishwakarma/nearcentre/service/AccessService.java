package com.vishwakarma.nearcentre.service;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vishwakarma.nearcentre.messaging.EmailHelper;
import com.vishwakarma.nearcentre.utils.NotificationHelper;

@Service
public class AccessService {

	@Autowired
	private NotificationHelper notificationHelper;
	
	@Resource
	private UserService userService;
	
	@Resource
	private EmailHelper emailHelper;

	private final static Logger log = LoggerFactory.getLogger(AccessService.class);
}
