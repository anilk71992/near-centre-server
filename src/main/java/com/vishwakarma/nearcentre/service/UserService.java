
package com.vishwakarma.nearcentre.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.model.UserInfo;
import com.vishwakarma.nearcentre.model.specifications.UserSpecifications;
import com.vishwakarma.nearcentre.repository.UserRepository;
import com.vishwakarma.nearcentre.utils.AppConstants;
import com.vishwakarma.nearcentre.utils.CommonUtils;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;
import com.vishwakarma.nearcentre.utils.Md5Util;
import com.vishwakarma.nearcentre.utils.NotificationHelper;
import com.vishwakarma.nearcentre.utils.RoleCache;

@Service
public class UserService {

	@Resource
	private UserRepository userRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Transactional
	public User createUser(User user, User loggedInUser, boolean isRegister) {
		
		if(null == user) {
			throw new NearCentreException("Invalid user");
		}
		String password= user.getPassword();
		if(StringUtils.isBlank(password)){
			password = RandomStringUtils.randomAlphanumeric(6);
		}
		user.setPassword(Md5Util.md5(password));
		user.setIsEnabled(true);
		user.setReferralCode(getReferralCode(user));
		user = userRepository.save(user);
				
		notificationHelper.sendNotificationForNewAccount(user, password);
		if(!isRegister) {
			return populateTransientFields(user, loggedInUser);
		}
		return user;
	}
	
	@Transactional
	public List<User> getAllUsers(User loggedInUser) {
		
		List<User> users = userRepository.findAll();
		if(null != loggedInUser){
			for (User user : users) {
				user = populateTransientFields(user, loggedInUser);
			}
		}
		return users;
	}
	
	@Transactional
	public Page<User> getUsers(Integer pageNumber, String role, User loggedInUser) {
		
		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(pageRequest);
		} else {
			page = userRepository.findUserByRole(roleCache.getRole(role).getId(), pageRequest);
		}
		Iterator<User> iterator = page.iterator();
		while (iterator.hasNext()) {
			User user = (User) iterator.next();
			user = populateTransientFields(user, loggedInUser);
		}
		return page;
	}
	
	@Transactional
	public List<User> getUsersForRole(String role, User loggedInUser) {
		List<User> users = userRepository.findByRole(roleCache.getRole(role).getId());
		if(null != loggedInUser)
		for (User user : users) {
			user = populateTransientFields(user, loggedInUser);
		}
		return users;
	}

	@Transactional
	public Page<User> searchUsers(Integer pageNumber, String searchTerm, String role, User loggedInUser) {
		
		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(UserSpecifications.search(searchTerm), pageRequest);
		}else{
			page = userRepository.findAll(UserSpecifications.searchBySearchTermAndRole(searchTerm, role),pageRequest);
		}
		Iterator<User> iterator = page.iterator();
		while (iterator.hasNext()) {
			User user = (User) iterator.next();
			user = populateTransientFields(user, loggedInUser);
		}
		return page;
	}
	
	/**
	 * returns @User with populating transient fields
	 * @param id
	 * @return
	 */
	public User getUser(Long id, boolean doPostValidation, User loggedInUser) {
		
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		User user = getUser(id, false);
		if(doPostValidation && null == user) {
			throw new NearCentreException("User with this Id does not exists");
		}
		return populateTransientFields(user, loggedInUser);
		
	}
	
	public User getUserByCode(String code, boolean doPostValidation, User loggedInUser) {
		
		if(StringUtils.isBlank(code)) {

			throw new NearCentreException("Invalid user code");
		}
		User user = userRepository.findByCode(code);
		if(doPostValidation && null == user) {
			throw new NearCentreException("User with this Id does not exists");
		}
		return populateTransientFields(user, loggedInUser);
		
	}
	
	public User getSuperUser() {
		User user = userRepository.findByRole(roleCache.getRole(Role.ROLE_SUPER_ADMIN).getId()).get(0);
		return user;
	}

	/**
	 * returns @User without populating transient fields
	 * @param id
	 * @return
	 */
	public User getUser(Long id, boolean doPostValidation) {
		
		User user;
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		user = userRepository.findOne(id);
		if(doPostValidation && null == user) {
			throw new NearCentreException("User with this Id does not exists");
		}
		return user;
	}

	/**
	 * returns @User without populating transient fields
	 * @param email
	 * @return
	 */
	public User getUserByEmail(String email, boolean doPostValidation) {
		
		User user;
		if(StringUtils.isBlank(email)) {
			throw new NearCentreException("Invalid email");
		}
		user = userRepository.findByEmail(email);
		if(doPostValidation && null == user) {
			throw new NearCentreException("User with this email does not exists");
		}
		return user;
	}
	
	/**
	 * returns @User without populating transient fields
	 * @param mobileNumber
	 * @return
	 */
	public User getUserByMobileNumber(String mobileNumber, boolean doPostValidation) {

		User user;
		if(StringUtils.isBlank(mobileNumber)) {
			throw new NearCentreException("Invalid mobile number.");
		}
		user = userRepository.findByMobileNumber(mobileNumber);
		if(doPostValidation && null == user) {
			throw new NearCentreException("User with this mobile number does not exists.");
		}
		return user;
	}
	
	/**
	 * @return <b>FIRST</b> user with matching referralCode, otherwise returns null
	 */
	public User getUserByReferralCode(String referralCode, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<User> users = userRepository.findByReferralCode(referralCode);
		if(users.isEmpty() && doPostValidation) {
			throw new NearCentreException("User with this referral code does not exists");
		} else if(users.isEmpty()) {
			return null;
		}
		
		User user = users.get(0);
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		
		return user;	
	}
	
	/**
	 * 
	 * Updates the user. Do not updates mobilenumber, email, password, roles & verification code
	 * 
	 * @param id - Id of existing user
	 * @param user - Updated User
	 * @param loggedInUser - LoggedIn User
	 * @return Updated User
	 * 
	 * @author Vishal
	 */
	public User updateUser(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new NearCentreException("User with this Id does not exists");
		}
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		
		if(StringUtils.isNotBlank(user.getVerificationCode())){
			existingUser.setVerificationCode(user.getVerificationCode());
		}
		existingUser.setAlterNateMobileNumber(user.getAlterNateMobileNumber());
		existingUser.setDob(user.getDob());
		existingUser.setGender(user.getGender());
		if(null != user.getReferredBy()){
			existingUser.setReferredBy(user.getReferredBy());
			existingUser.setReferredMedia(user.getReferredMedia());
		}
		if(StringUtils.isNotBlank(user.getReferralCode())){
			existingUser.setReferralCode(user.getReferralCode());
		}
		if(null != user.getProfilePicData()){
			existingUser.setProfilePicData(user.getProfilePicData());
			existingUser.setFileName(user.getFileName());
		}
		existingUser.createProfilePicFile();
		
		if(user.getUserRoles().size()>0)
			existingUser.getUserRoles().clear();
		for (Role role : user.getUserRoles()) {
			existingUser.getUserRoles().add(role);
		}
		existingUser.setEnabled(user.getIsEnabled());
		existingUser = userRepository.save(existingUser);
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));

		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	public User updateLocation(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new NearCentreException("User with this Id does not exists");
		}
		existingUser.setLat(user.getLat());
		existingUser.setLng(user.getLng());
	
		existingUser = userRepository.save(existingUser);
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	public User updatePasswordForUser(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);

		if(null == existingUser) {
			throw new NearCentreException("User with this Id does not exists");
		}
		
		existingUser.setPassword(user.getPassword());
		existingUser = userRepository.save(existingUser);
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));

		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public void deleteUser(Long id, User loggedInUser) {
		
		if(null == id) {
			throw new NearCentreException("Invalid user id");
		}
		User user = userRepository.findOne(id);
		if(null == user) {
			throw new NearCentreException("User with this Id does not exists");
		}
		userRepository.delete(id);
	}
	
	public boolean checkPassword(Long userId, String password) {
		User user = userRepository.findOne(userId);
		return Md5Util.md5(password).equals(user.getPassword());
	}
	
	public void changePassword(String newPassword) {
		
		UserInfo userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userRepository.findOne(userInfo.getId());
		if(null == user) {
			throw new NearCentreException("User not found");
		}
		user.setPassword(Md5Util.md5(newPassword));
		userRepository.save(user);
	}

	public String getReferralCode(User user){
		
		String referralCode=CommonUtils.getReferralCodeForUser(user);
		if(StringUtils.isNotBlank(referralCode)){
			List<User> existingUsers=userRepository.findByReferralCode(referralCode);
			if(existingUsers.size()>0){
				return getReferralCode(user);
			}
		}
		
		return referralCode;
	}
	
	public User getLoggedInUser() {
		
		UserInfo userInfo = getLoggedInUserInfo();
		if(null == userInfo) {
			throw new NearCentreException("Invalid logged-in user information");
		}
		User user = getUser(userInfo.getId(), false);
		return user;
	}

	/**
	 * @return
	 */
	public UserInfo getLoggedInUserInfo() {
		UserInfo userInfo;
		try {
			userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			e.printStackTrace();
			throw new NearCentreException("Error retrieving logged in user information");
		}
		return userInfo;
	}
	
	private User fetchEagerly(User user) {
		user.getUserRoles().size();
		return user;
	}
	
	private User populateTransientFields(User user, User loggedInUser) {
		
		if(null == user) {
			return user;
		}
		if(null == loggedInUser) {
			throw new NearCentreException("Invalid user");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			user.setIsEditAllowed(true);
			user.setIsDeleteAllowed(true);
		} else {
			user.setIsEditAllowed(false);
			user.setIsDeleteAllowed(false);
		}
		user.setCipher(EncryptionUtil.encode(user.getId()+""));
		return user;
	}
}
