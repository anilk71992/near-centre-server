package com.vishwakarma.nearcentre.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.ServiceCentre;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.model.specifications.ServiceCentreSpecifications;
import com.vishwakarma.nearcentre.repository.ServiceCentreRepository;
import com.vishwakarma.nearcentre.utils.AppConstants;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;


@Service
public class ServiceCentreService {

	@Resource
	private ServiceCentreRepository serviceCentreRepository;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public ServiceCentre createServiceCentre(ServiceCentre serviceCentre, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == serviceCentre) {
			throw new NearCentreException("Invalid serviceCentre");
		}
		serviceCentre = serviceCentreRepository.save(serviceCentre);
		activityLogService.createActivityLog(loggedInUser, "add", "ServiceCentre", serviceCentre.getId());
		if (populateTransientFields) {
			serviceCentre = populateTransientFields(serviceCentre, loggedInUser);
		}

		if(fetchEagerly) {
			serviceCentre= fetchEagerly(serviceCentre);
		}
		return serviceCentre;
	}
	
	@Transactional
	public List<ServiceCentre> getAllServiceCentres(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<ServiceCentre> serviceCentres = serviceCentreRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (ServiceCentre serviceCentre : serviceCentres) {
				if(populateTransientFields)
					serviceCentre = populateTransientFields(serviceCentre, loggedInUser);
				if (fetchEagerly)
					serviceCentre = fetchEagerly(serviceCentre);
			}
		}
		return serviceCentres;
	}
	
	@Transactional
	public Page<ServiceCentre> getServiceCentres(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<ServiceCentre> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = serviceCentreRepository.findAll(pageRequest);
		Iterator<ServiceCentre> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				ServiceCentre serviceCentre = (ServiceCentre) iterator.next();
				if(populateTransientFields)
					serviceCentre = populateTransientFields(serviceCentre, loggedInUser);
				if(fetchEagerly)
					serviceCentre = fetchEagerly(serviceCentre);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<ServiceCentre> searchServiceCentre(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<ServiceCentre> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = serviceCentreRepository.findAll(ServiceCentreSpecifications.search(searchTerm), pageRequest);
		Iterator<ServiceCentre> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				ServiceCentre serviceCentre = (ServiceCentre) iterator.next();
				if(populateTransientFields)
					serviceCentre = populateTransientFields(serviceCentre, loggedInUser);
				if(fetchEagerly)
					serviceCentre = fetchEagerly(serviceCentre);
			}
		}
		return page;
	}
	
	public ServiceCentre getServiceCentre(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid serviceCentre id");
		}
		ServiceCentre serviceCentre = serviceCentreRepository.findOne(id);
		if(doPostValidation && null == serviceCentre) {
			throw new NearCentreException("ServiceCentre with this id does not exists");
		}
		if(null == serviceCentre){
			return null;
		}
		if(populateTransientFields)
			serviceCentre= populateTransientFields(serviceCentre, loggedInUser);
		if(fetchEagerly)
			serviceCentre= fetchEagerly(serviceCentre);
		
		return serviceCentre;
	}
	
	public ServiceCentre getServiceCentreByName(String serviceCentreName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(serviceCentreName)) {
			throw new NearCentreException("Invalid serviceCentre name");
		}
		ServiceCentre serviceCentre = serviceCentreRepository.findByServiceCentreName(serviceCentreName);
		if(doPostValidation && null == serviceCentre) {
			throw new NearCentreException("ServiceCentre with this name does not exists");
		}
		if(serviceCentre == null){
			return null;
		}
		if(populateTransientFields)
			serviceCentre= populateTransientFields(serviceCentre, loggedInUser);
		if(fetchEagerly)
			serviceCentre= fetchEagerly(serviceCentre);
		
		return serviceCentre;
	}
	
	/**
	 * 
	 * Updates the ServiceCentre.
	 * @param id - Id of existing ServiceCentre
	 * @param ServiceCentre - Updated ServiceCentre
	 * @return Updated ServiceCentre
	 */
	public ServiceCentre updateServiceCentre(Long id, ServiceCentre serviceCentre, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid ServiceCentre id");
		}
		ServiceCentre existingServiceCentre = serviceCentreRepository.findOne(id);
		
		if(null == existingServiceCentre) {
			throw new NearCentreException("ServiceCentre with this id does not exists");
		}
		existingServiceCentre.setServiceCentreName(serviceCentre.getServiceCentreName());
		existingServiceCentre.setServiceCentreAddress(serviceCentre.getServiceCentreAddress());
		existingServiceCentre.setServiceCentreLatitude(serviceCentre.getServiceCentreLatitude());
		existingServiceCentre.setServiceCentreLongitude(serviceCentre.getServiceCentreLongitude());
		existingServiceCentre.setServiceCentrePostalCode(serviceCentre.getServiceCentrePostalCode());
		existingServiceCentre.setCity(serviceCentre.getCity());
		existingServiceCentre.setServiceCentreStartTime(serviceCentre.getServiceCentreStartTime());
		existingServiceCentre.setServiceCentreEndTime(serviceCentre.getServiceCentreEndTime());

		existingServiceCentre = serviceCentreRepository.save(existingServiceCentre);
		activityLogService.createActivityLog(loggedInUser, "update", "ServiceCentre",id);
		existingServiceCentre.setCipher(EncryptionUtil.encode(serviceCentre.getId()+""));
		if(populateTransientFields)
			existingServiceCentre =populateTransientFields(existingServiceCentre, loggedInUser);
		if(fetchEagerly)
			existingServiceCentre=fetchEagerly(existingServiceCentre);
		return existingServiceCentre;
	}
	
	

	
	public void deleteServiceCentre(Long id) {
		
		if(null == id) {
			throw new NearCentreException("Invalid serviceCentre id");
		}
		ServiceCentre serviceCentre = serviceCentreRepository.findOne(id);
		if(null == serviceCentre) {
			throw new NearCentreException("ServiceCentre with this id does not exists");
		}
		serviceCentreRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "ServiceCentre",id);
	}
	
	private ServiceCentre populateTransientFields(ServiceCentre serviceCentre, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new NearCentreException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			serviceCentre.setIsEditAllowed(true);
			serviceCentre.setIsDeleteAllowed(true);
		} else {
			serviceCentre.setIsEditAllowed(false);
			serviceCentre.setIsDeleteAllowed(false);
		}
		serviceCentre.setCipher(EncryptionUtil.encode(serviceCentre.getId()+""));
		return serviceCentre;
	}
	
	private ServiceCentre fetchEagerly(ServiceCentre serviceCentre) {
		
		return serviceCentre;
	}

	
}