package com.vishwakarma.nearcentre.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.nearcentre.exception.NearCentreException;
import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.Role;
import com.vishwakarma.nearcentre.model.User;
import com.vishwakarma.nearcentre.model.specifications.BrandSpecifications;
import com.vishwakarma.nearcentre.repository.BrandRepository;
import com.vishwakarma.nearcentre.utils.AppConstants;
import com.vishwakarma.nearcentre.utils.EncryptionUtil;


@Service
public class BrandService {

	@Resource
	private BrandRepository brandRepository;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public Brand createBrand(Brand brand, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == brand) {
			throw new NearCentreException("Invalid brand");
		}
		brand = brandRepository.save(brand);
		activityLogService.createActivityLog(loggedInUser, "add", "Brand", brand.getId());
		if (populateTransientFields) {
			brand = populateTransientFields(brand, loggedInUser);
		}

		if(fetchEagerly) {
			brand= fetchEagerly(brand);
		}
		return brand;
	}
	
	@Transactional
	public List<Brand> getAllBrands(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<Brand> brands = brandRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (Brand brand : brands) {
				if(populateTransientFields)
					brand = populateTransientFields(brand, loggedInUser);
				if (fetchEagerly)
					brand = fetchEagerly(brand);
			}
		}
		return brands;
	}
	
	@Transactional
	public Page<Brand> getBrands(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Brand> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = brandRepository.findAll(pageRequest);
		Iterator<Brand> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Brand brand = (Brand) iterator.next();
				if(populateTransientFields)
					brand = populateTransientFields(brand, loggedInUser);
				if(fetchEagerly)
					brand = fetchEagerly(brand);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Brand> searchBrand(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Brand> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = brandRepository.findAll(BrandSpecifications.search(searchTerm), pageRequest);
		Iterator<Brand> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Brand brand = (Brand) iterator.next();
				if(populateTransientFields)
					brand = populateTransientFields(brand, loggedInUser);
				if(fetchEagerly)
					brand = fetchEagerly(brand);
			}
		}
		return page;
	}
	
	public Brand getBrand(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid brand id");
		}
		Brand brand = brandRepository.findOne(id);
		if(doPostValidation && null == brand) {
			throw new NearCentreException("Brand with this id does not exists");
		}
		if(null == brand){
			return null;
		}
		if(populateTransientFields)
			brand= populateTransientFields(brand, loggedInUser);
		if(fetchEagerly)
			brand= fetchEagerly(brand);
		
		return brand;
	}
	
	public Brand getBrandByName(String brandName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(brandName)) {
			throw new NearCentreException("Invalid brand name");
		}
		Brand brand = brandRepository.findByBrandName(brandName);
		if(doPostValidation && null == brand) {
			throw new NearCentreException("Brand with this name does not exists");
		}
		if(brand == null){
			return null;
		}
		if(populateTransientFields)
			brand= populateTransientFields(brand, loggedInUser);
		if(fetchEagerly)
			brand= fetchEagerly(brand);
		
		return brand;
	}
	
	/**
	 * 
	 * Updates the Brand.
	 * @param id - Id of existing Brand
	 * @param Brand - Updated Brand
	 * @return Updated Brand
	 */
	public Brand updateBrand(Long id, Brand brand, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new NearCentreException("Invalid Brand id");
		}
		Brand existingBrand = brandRepository.findOne(id);
		
		if(null == existingBrand) {
			throw new NearCentreException("Brand with this id does not exists");
		}
		existingBrand.setBrandName(brand.getBrandName());
		existingBrand.setBrandColor(brand.getBrandColor());
		existingBrand = brandRepository.save(existingBrand);
		activityLogService.createActivityLog(loggedInUser, "update", "Brand",id);
		existingBrand.setCipher(EncryptionUtil.encode(brand.getId()+""));
		if(populateTransientFields)
			existingBrand =populateTransientFields(existingBrand, loggedInUser);
		if(fetchEagerly)
			existingBrand=fetchEagerly(existingBrand);
		return existingBrand;
	}
	
	public void deleteBrand(Long id) {
		
		if(null == id) {
			throw new NearCentreException("Invalid brand id");
		}
		Brand brand = brandRepository.findOne(id);
		if(null == brand) {
			throw new NearCentreException("Brand with this id does not exists");
		}
		brandRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Brand",id);
	}
	
	private Brand populateTransientFields(Brand brand, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new NearCentreException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			brand.setIsEditAllowed(true);
			brand.setIsDeleteAllowed(true);
		} else {
			brand.setIsEditAllowed(false);
			brand.setIsDeleteAllowed(false);
		}
		brand.setCipher(EncryptionUtil.encode(brand.getId()+""));
		return brand;
	}
	
	private Brand fetchEagerly(Brand brand) {
		
		return brand;
	}
	
}