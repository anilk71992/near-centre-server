package com.vishwakarma.nearcentre.repository;

import com.vishwakarma.nearcentre.model.Privilege;


public interface PrivilegeRepository extends CustomRepository<Privilege, Long> {

	public Privilege findByName(final String name);
	
}
