package com.vishwakarma.nearcentre.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.nearcentre.model.City;


public interface CityRepository extends JpaRepository<City, Long>,JpaSpecificationExecutor<City> {
	
	public City findByCode(String code);
	
	public City findByName(String name);
	
	public List<City>findByState(Long id);
	
	public City findByNameAndCountryName(String name,String countryName);
	
	public List<City> findByCountry_Code(String countryCode);
	
}