package com.vishwakarma.nearcentre.repository;

import com.vishwakarma.nearcentre.model.Country;


public interface CountryRepository extends CustomRepository<Country, Long>{

	public Country findByCode(String code);
	
	public Country findByName(String name);
}

