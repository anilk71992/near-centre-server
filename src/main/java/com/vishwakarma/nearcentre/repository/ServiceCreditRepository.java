package com.vishwakarma.nearcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.nearcentre.model.ServiceCredit;

public interface ServiceCreditRepository extends JpaRepository<ServiceCredit, Long> {
	public ServiceCredit findByServiceName(String serviceName);
}

