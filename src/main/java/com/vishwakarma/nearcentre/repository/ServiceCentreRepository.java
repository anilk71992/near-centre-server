package com.vishwakarma.nearcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.ServiceCentre;


public interface ServiceCentreRepository extends JpaRepository<ServiceCentre,Long>,JpaSpecificationExecutor<ServiceCentre>{
	
	public ServiceCentre findByCode(String code);
	
	public ServiceCentre findByServiceCentreName(String serviceCentreName);
}