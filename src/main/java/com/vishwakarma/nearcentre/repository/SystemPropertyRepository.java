package com.vishwakarma.nearcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.nearcentre.model.SystemProperty;

public interface SystemPropertyRepository extends JpaRepository<SystemProperty, Long> {
	public SystemProperty findByPropName(String propName);
}

