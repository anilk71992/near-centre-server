package com.vishwakarma.nearcentre.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vishwakarma.nearcentre.model.User;

public interface UserRepository extends JpaRepository<User, Long>,JpaSpecificationExecutor<User> {
	
	public User findByEmail(String email);
	
	public User findByMobileNumber(String mobileNumber);

	public User findByCode(String code);
	
	public List<User> findByReferralCode(String referralCode);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId order by u.id asc")
	public List<User> findByRole(@Param("roleId") Long roleId);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id not in :roleIds")
	public List<User> findByRolesNotIn(@Param("roleIds") List<Long> roleIds, Pageable p);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id in :roleIds order by u.id asc")
	public List<User> findByRoles(@Param("roleIds") List<Long> roleIds);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId order by u.id asc")
	public Page<User> findUserByRole(@Param("roleId") Long roleId, Pageable pageable);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where NOT role.id = :roleId order by u.id asc")
	public Page<User> findUserByRoleNotEqual(@Param("roleId") Long roleId, Pageable pageable);
}

