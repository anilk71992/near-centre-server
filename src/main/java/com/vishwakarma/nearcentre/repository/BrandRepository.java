package com.vishwakarma.nearcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.nearcentre.model.Brand;



public interface BrandRepository extends JpaRepository<Brand,Long>,JpaSpecificationExecutor<Brand>{
	
	public Brand findByCode(String code);
	
	public Brand findByBrandName(String brandName);

}
