package com.vishwakarma.nearcentre.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.nearcentre.model.Product;

import com.vishwakarma.nearcentre.model.Brand;
import com.vishwakarma.nearcentre.model.City;;


public interface ProductRepository extends JpaRepository<Product,Long>, JpaSpecificationExecutor<Product>{

	public Product findByCode(String code);
	
	public Product findByProductName(String productName);
	
	//public Product findByProductNameAndBrandName(String productName,String brandName);
}
