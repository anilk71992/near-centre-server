package com.vishwakarma.nearcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.nearcentre.model.MessageLog;

public interface MessageLogRepository extends JpaRepository<MessageLog, Long> {

}

