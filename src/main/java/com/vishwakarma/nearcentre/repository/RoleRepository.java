package com.vishwakarma.nearcentre.repository;


import org.springframework.data.repository.CrudRepository;

import com.vishwakarma.nearcentre.model.Role;


public interface RoleRepository extends CrudRepository<Role, Long> {

	public Role findByRole(final String roleName);
	
	
}
