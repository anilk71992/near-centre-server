<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ include file="/taglibs.jsp"%>

<html>
<head>
	<title>Login</title>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link  href="<c:url value='/assets/end-user/css/signin-page.min.css'/>" rel="stylesheet">
</head>

<body>
	<sec:authorize access="isAuthenticated()">
		<c:redirect url="/home"/>
	</sec:authorize>
    
    <%-- <div class="signin-container">
        <!-- Right side -->
        <div class="signin-form">
            <!-- Form -->
            <form action="index.html" id="signin-form_id">
                <div class="signin-text">
                    <span>Sign In to your account</span>
                </div>
                <!-- / .signin-text -->
                <div class="form-group w-icon">
                    <input type="text" name="signin_username" id="username_id" class="form-control input-lg"
                    placeholder="Username or email" />
                </div>
                <!-- / Username -->
                <div class="form-group w-icon">
                    <input type="password" name="signin_password" id="password_id" class="form-control input-lg"
                    placeholder="Password" />
                </div>
                <!-- / Password -->
                <div class="form-actions">
                <input type="submit" value="SIGN IN" class="signin-btn bg-primary" /> 
                <a href="#" class="forgot-password" id="forgot-password-link">Forgot your password?</a></div>
                <!-- / .form-actions -->
            </form>
            <!-- / Form -->
            <!-- "Sign In with" block -->
            <div class="signin-with">
                <!-- Facebook -->
                <a href="index.html" class="signin-with-btn" style="background:#4f6faa;background:rgba(79, 111, 170, .8);">Sign In
                with 
                <span>Facebook</span></a>
            </div>
            <!-- / "Sign In with" block -->
            <!-- Password reset form -->
            <div class="password-reset-form" id="password-reset-form">
                <div class="header">
                    <div class="signin-text">
                        <span>Password reset</span>
                        <div class="close">X/div>
                    </div>
                    <!-- / .signin-text -->
                </div>
                <!-- / .header -->
                <!-- Form -->
                <form action="index.html" id="password-reset-form_id">
                    <div class="form-group w-icon">
                        <input type="text" name="password_reset_email" id="p_email_id" class="form-control input-lg"
                        placeholder="Enter your email" />
                    </div>
                    <!-- / Email -->
                    <div class="form-actions">
                        <input type="submit" value="SEND PASSWORD RESET LINK" class="signin-btn bg-primary" />
                    </div>
                    <!-- / .form-actions -->
                </form>
                <!-- / Form -->
            </div>
            <!-- / Password reset form -->
        </div>
        <!-- Right side -->
    </div> --%>
    <div style='float: right;'><a href="<c:url value="/assets/apk/NearCentre-Internal-App.apk" />">download NearCentre - Internal App</a></div>
	<div class="col-lg-6">
	
		<div class="well bs-component">
			<form action="j_spring_security_check" class="form-horizontal" method="post" >
				<fieldset>
					<legend>Login</legend>
					<div class="form-group">
	                    <label for="username" class="col-lg-2 control-label">Username</label>
	                    <div class="col-lg-10">
	                      <input class="form-control" id="username" name="j_username" type="text" placeholder="Enter Username">
	                    </div>
	                 </div>
	                 <div class="form-group">
	                    <label for="password" class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-10">
	                      <input class="form-control" id="password" name="j_password" type="password" placeholder="***********">
	                    </div>
	                 </div>
					<div class="form-group">
		               <div class="col-lg-10 col-lg-offset-2">
		                 <input class="btn btn-primary" type="submit" value="Login"/>
		               </div>
		             </div>
				</fieldset>
				<a href="<c:url value="/admin/access/forgotPassword" />">Recover my password</a>
			</form>
		</div>
	</div>
</body>
</html>