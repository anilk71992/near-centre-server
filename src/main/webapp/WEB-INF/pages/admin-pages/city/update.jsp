<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Update City</title>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#country').select2();
	
});


function submitForm() {
	if(validate())
		$('#updateCity').submit();
}

function validate() {
	
	var countryValue = $('#country').val();
	if (! countryValue|| countryValue == -1 ) {
		showToast("warning", "Please select country");
		return false;
	}
	
	var city =$('#name').val(), chardig= /^[a-zA-Z ]*$/;
	var cityVal=city.trim();
	if(!chardig.test(city)|| (!cityVal)){
		showToast('warning','Please Enter Valid City');
		return false;
		}
	
	var state =$('#state').val().trim(), chardig= /^[a-zA-Z ]*$/;
	if(!chardig.test(state)){
		showToast('warning','Please Enter Valid State');
		return false;
		}
	
	$('#name').val(cityVal);
	return true;
}
</script>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/city/update" var="updateCity"/>

	<form:form id="updateCity" method="post" modelAttribute="city" action="${updateCity}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
			<h3 class="page-header">City</h3>
			<div class="input-group width-xlarge">
				<form:select path="country" class="form-control">
				<option value="-1"> -- Select Countries-- </option>
				<c:forEach items="${countries}" var="country">
					<option value="${country.id}"<c:if test="${country.id == city.country.id}">selected="selected"</c:if>>${country.name}</option>
				</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Name*</span> 
				<form:input path="name" cssClass="form-control"/>
			</div>
			<br>
			<form:errors path="state" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">State</span> 
				<form:input path="state" cssClass="form-control"/>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>