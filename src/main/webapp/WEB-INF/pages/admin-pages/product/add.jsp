<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Add Product</title>
<script type="text/javascript">
	$(document).ready(function() {
	});
	
	function submitForm() {
		if (validate())
			$('#addProduct').submit();
	}
	
	$(document).ready(function() {
		
		$('#brandName').select2();
		
	});
	
	function validate()
	{
		
		var product = $('#productName').val().trim(), chardig= /^[a-zA-Z ]*$/;
		
		if(!chardig.test(product))
			{
			showToast('warning','Please enter Valid Product in Characters');
			return false;
			}
		else if(!product)
			{
			showToast('warning','Please enter Valid Product');
			return false;
			}
		return true;
		
	}

</script>
</head>
<c:url value="/admin/product/add" var="productAdd" />
<c:url value="/admin/product/list" var="productList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/product/add" var="addProduct" />

	<form:form id="addProduct" action="${productAdd}" method="post"
		modelAttribute="product" enctype="application/x-www-form-urlencoded">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Product </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
			<fieldset>
			
			
			<form:errors path="brand" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Brand Name *</span>
				<form:select path="brand" id ="brandName" cssClass="form-control" >
				<option value="-1"> --Select brand --</option>
				<c:forEach items="${brands}" var="brand">
				<form:option value="${brand.id}"> ${brand.brandName}
				</form:option>
				</c:forEach>
				</form:select>
				
			</div>
		

			<form:errors path="productName" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name *</span>
				<form:input path="productName" id ="productName" cssClass="form-control" />
			</div>
			<br>
			

		</fieldset>
		</div>	
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:submitForm()">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>