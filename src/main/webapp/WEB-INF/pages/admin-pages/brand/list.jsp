<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Brand</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Brand</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Brand</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/brand/list" />
	<c:set var="urlPrefix" value="/admin/brand/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/brand/show" var="brandDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="brands" class="footable" requestURI="" id="brand" export="false">
			<display:column title="Code" sortable="true">
				<a href="${brandDetailLinkPrefix}/${brand.cipher}">${brand.code}</a>
			</display:column>
			
			<display:column title="Name" sortable="true">${brand.brandName}</display:column>
			<display:column title="Colour" sortable="true">${brand.brandColor}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/brand/update/${brand.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/brand/delete/${brand.cipher}">
							<li><a href="#ConfirmModal_${brand.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${brand.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Brand?</h3>
							</div>
							<div class="modal-body">
								<p>The Brand will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/brand/delete/${brand.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

</body>
</html>