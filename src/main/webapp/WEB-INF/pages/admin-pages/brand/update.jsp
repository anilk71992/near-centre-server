<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Update Brand</title>
<script type="text/javascript">
$(document).ready(function() {
});

function submitForm() {
	if (validate())
		$('#updateBrand').submit();
}

function validate()
{
	
	var brand = $('#brandName').val(), chardig= /^[a-zA-Z ]*$/;
	
	if(!chardig.test(brand))
		{
		showToast('warning','Please enter Valid Brand in Characters');
		return false;
		}
	return true;
	
}
</script>
</head>

<c:url value="/admin/brand/update" var="brandUpdate" />
<c:url value="/admin/brand/list" var="brandList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/brand/update" var="updateBrand"/>
	<form:form id="updateBrand" method="post" modelAttribute="brand" action="${updateBrand}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Brand </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
			<fieldset>

			<form:errors path="brandName" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name *</span>
				<form:input path="brandName" id ="brandName" cssClass="form-control" />
			</div>
			<br>
			
			<form:errors path="brandColor" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Brand Color *</span>
				<form:input path="brandColor" id ="brandColor" cssClass="form-control" />
			</div>
			<br>

		</fieldset>
		</div>	
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:submitForm()">Update</a>	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>