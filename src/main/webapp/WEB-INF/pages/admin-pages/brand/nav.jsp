<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/brand/add" var="brandAdd" />
<c:url value="/admin/brand/list" var="brandList" />
<body>

<div class="row border-bottom white-bg dashboard-header">
	<div class="span4">
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN')">
	<a class="btn btn-primary" href="${brandAdd}">Add New Brand </a>&nbsp;
	</sec:authorize>
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>