<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NearCentre - ServiceCentre</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">ServiceCentre- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${serviceCentre.serviceCentreName}</p>
				
			</div>
				
			<div class="row">
				<label class="col-md-2">Address</label>
				<p class="col-md-2">${serviceCentre.serviceCentreAddress}</p>
				
			</div>
			
				
			<div class="row">
				<label class="col-md-2">State</label>
				<p class="col-md-2">${serviceCentre.serviceCentreState}</p>
				
			</div>
			
			<div class="row">
				<label class="col-md-2">Postal Code</label>
				<p class="col-md-2">${serviceCentre.serviceCentrePostalCode}</p>
				
			</div>
			
				
			<div class="row">
				<label class="col-md-2">Latitude</label>
				<p class="col-md-2">${serviceCentre.serviceCentreLatitude}</p>
				
			</div>
			
				
			<div class="row">
				<label class="col-md-2">Longitude</label>
				<p class="col-md-2">${serviceCentre.serviceCentreLongitude}</p>
				
			</div>
			
			
			<div class="row">
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${serviceCentre.serviceCentreTimings}</p>
				
			</div>
			
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${serviceCentre.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${serviceCentre.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${serviceCentre.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${serviceCentre.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>