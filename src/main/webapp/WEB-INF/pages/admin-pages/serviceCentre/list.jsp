<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - ServiceCentre</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>ServiceCentre</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>ServiceCentre</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/serviceCentre/list" />
	<c:set var="urlPrefix" value="/admin/serviceCentre/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/serviceCentre/show" var="serviceCentreDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="serviceCentres" class="footable" requestURI="" id="serviceCentre" export="false">
			<display:column title="Code" sortable="true">
				<a href="${serviceCentreDetailLinkPrefix}/${serviceCentre.cipher}">${serviceCentre.code}</a>
			</display:column>
			
			<display:column title="Name" sortable="true">${serviceCentre.serviceCentreName}</display:column>
			<display:column title="Address" sortable="true">${serviceCentre.serviceCentreAddress}</display:column>
			<display:column title="City" sortable="true">${serviceCentre.city.name}</display:column>
			<display:column title="PostalCode" sortable="true">${serviceCentre.serviceCentrePostalCode}</display:column>
			<display:column title="Latitude" sortable="true">${serviceCentre.serviceCentreLatitude}</display:column>
			<display:column title="Longitude" sortable="true">${serviceCentre.serviceCentreLongitude}</display:column>
			<display:column title="Timings" sortable="true">${serviceCentre.serviceCentreStartTime} AM  - ${serviceCentre.serviceCentreEndTime} PM</display:column>
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/serviceCentre/update/${serviceCentre.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/serviceCentre/delete/${serviceCentre.cipher}">
							<li><a href="#ConfirmModal_${serviceCentre.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${serviceCentre.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete ServiceCentre?</h3>
							</div>
							<div class="modal-body">
								<p>The ServiceCentre will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/serviceCentre/delete/${serviceCentre.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

</body>
</html>