<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Update ServiceCentre</title>
<script type="text/javascript">
$(document).ready(function() {
});

function submitForm() {
	if (validate())
		$('#updateServiceCentre').submit();
}

function validationOfLatitude()
{
	var value;
	value= $("#serviceCentreLatitude").val();
	$.ajax({
		url:'<c:url value="restapi/value/double"/>',
		datatype:'json',
		data: 'value= '+value,
		success: function(response){
			if(response.success){
				showToast("warning","Please enter valid latitude");
				$("serviceCentreLatitude").val("");
				$("serviceCentreLatitude").focus();
;
				}
		}
	});
}

function validationOfLongitude()
{
	var value;
value=$("#serviceCentreLongitude").val();
	$.ajax({
		url:'<c:url value="resapi/value/double"/>',
		datatype:'json',
		data: 'value= '+value,
		success: function(response){
			if(response.success){
				showToast("warning","Please enter valid longitude");
				$("#serviceCentreLongitude").val("");
				$("#serviceCentreLongitude").focus();
			}
		}
	});
}

function checkOfValidDouble(value){
	var valueValid = parseFloat(value);
	var regExp = /^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/;
	
	if(value == ""){
		showToast('warning','Please enter value');	
		return false;
	} else if (!regExp.test(value)) { 		
		showToast( 'warning','Please enter valid price value in the format of floating point value');	
		return false;
	} else if (valueValid == '0' || valueValid == '0.0') { 		
		showToast( 'warning','Please enter value more than zero (0)');
		return false;
	}
	
	return true;
}


function validate()
{
	
	var lat;
	var lng;
	lat = $('#serviceCentreLatitude').val().trim();
	if(!checkOfValidDouble(lat)){
		$('#serviceCentreLatitude').focus();
		return false;
	}
	
	lng=$('#serviceCentreLongitude').val().trim();
	if(!checkOfValidDouble(lng))
		{
			$('#serviceCentreLongitude').focus();
			return false;
		}
	
	
	
	var serviceCentre = $('#serviceCentreName').val().trim(), chardig= /^[a-zA-Z ]*$/;
	
	
	
	if(!chardig.test(serviceCentre))
		{
		showToast('warning','Please enter Valid ServiceCentre in Characters');
		return false;
		}
	else if(!serviceCentre)
		{
		showToast('warning','Please enter Valid ServiceCentre');
		return false;
		}
	return true;
	
}

</script>
</head>

<c:url value="/admin/serviceCentre/update" var="serviceCentreUpdate" />
<c:url value="/admin/serviceCentre/list" var="serviceCentreList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/serviceCentre/update" var="updateServiceCentre"/>
	<form:form id="updateServiceCentre" method="post" modelAttribute="serviceCentre" action="${updateServiceCentre}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">ServiceCentre </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
			<fieldset>

	
			<form:errors path="serviceCentreName" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name *</span>
				<form:input path="serviceCentreName" id ="serviceCentreName" cssClass="form-control" />
			</div>
			<form:errors path="serviceCentreAddress" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Address *</span>
				<form:input path="serviceCentreAddress" id ="serviceCentreAddress" cssClass="form-control" />
			</div>
			<form:select path="city" class="form-control">
				<option value="-1"> -- Select City-- </option>
				<c:forEach items="${cities}" var="city">
					<option value="${city.id}"<c:if test="${city.id == serviceCentre.city.id}">selected="selected"</c:if>>${city.name}</option>
				</c:forEach>
				</form:select>
			</div>
				<form:errors path="serviceCentrePostalCode" cssClass="text-danger" />
			<div class="input-group width-xlarge">   
				<span class="input-group-addon"> Postal code *</span>
				<form:input path="serviceCentrePostalCode" id ="serviceCentrePostalCode" cssClass="form-control" />
				</div>
				<form:errors path="serviceCentreLatitude" cssClass="text-danger" />
			<div class="input-group width-xlarge">    
			<span class="input-group-addon"> Latitude *</span>
			<form:input path="serviceCentreLatitude" id ="serviceCentreLatitude" onchange="validationOfLatitude()" cssClass="form-control" />
			</div>
			
			<form:errors path="serviceCentreLongitude" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				
				<span class="input-group-addon"> Longitude *</span>
				<form:input path="serviceCentreLongitude" id ="serviceCentreLongitude"  onchange="validationOfLongitude()" cssClass="form-control" />
			</div>
			<form:errors path="serviceCentreStartTime" cssClass="text-danger" />
			<div class="input-group width-xlarge">
			<span class="input-group-addon">Start Time *</span>
				<form:input path="serviceCentreStartTime" id ="serviceCentreStartTime" cssClass="form-control" />
			
			</div>
			<form:errors path="serviceCentreEndTime" cssClass="text-danger" />
			<div class="input-group width-xlarge">
			<span class="input-group-addon">Start Time *</span>
				<form:input path="serviceCentreEndTime" id ="serviceCentreEndTime" cssClass="form-control" />
			
			</div>

		</fieldset>
		</div>	
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:submitForm()">Update</a>	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>