<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Add ServiceCentre</title>
<%@ include file="../underscore-templates.tmpl" %>
<link href="<c:url value="/assets/common-css/jHtmlArea.css"/>" rel="stylesheet">
<script src="<c:url value="/assets/common-js/jHtmlArea-0.8.js"/>"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#serviceCentreStartTime').timepicker('setTime','9:00 AM');
	$('#serviceCentreStartTime').timepicker('setTime','4:00 PM');
});

function submitForm() {
	if (validate())
		$('#addServiceCentre').submit();
}


function onStateSelect()
{
	var city =document.getElementById('state');
	var id= city.options[city.selectedIndex].getAttribute('value');
	
	
					
	
	$.ajax({
		url : '<c:url value="/restapi/existingCities/list"/>',
		datatype : 'json',
		data : 'id=' + id,
		success : function(response){
			if(response.success){
				existingCities = response.data.existingCities;
				if(existingCities.length != 0){
					cityMap1 =new Object();
					$('.CityClass').html("");
					
					$('.CityClass').append($('<option>',{
						value : "-1",
						text : "--select--"
					}));
					
					for(var i= 0; i < existingCities.length; i++){
						var city= existingCities[i];
						
						$('.CityClass').append($('<option>', {
							value : city.id,
							text : city.name
						}));
						cityMap1[city.id] = city;
					}
				} else {
					$('.CityClass').html("");
					toastr.clear();
					showToast("warning", "No city found !!");
				}
										
			}
		}
		
	});
}


	function validationOfLatitude()
	{
		var value;
		value= $("#serviceCentreLatitude").val();
		$.ajax({
			url:'<c:url value="restapi/value/double"/>',
			datatype:'json',
			data: 'value= '+value,
			success: function(response){
				if(response.success){
					showToast("warning","Please enter valid latitude");
					$("serviceCentreLatitude").val("");
					$("serviceCentreLatitude").focus();
;
					}
			}
		});
	}
	
	function validationOfLongitude()
	{
		var value;
	value=$("#serviceCentreLongitude").val();
		$.ajax({
			url:'<c:url value="resapi/value/double"/>',
			datatype:'json',
			data: 'value= '+value,
			success: function(response){
				if(response.success){
					showToast("warning","Please enter valid longitude");
					$("#serviceCentreLongitude").val("");
					$("#serviceCentreLongitude").focus();
				}
			}
		});
	}
	
	function checkOfValidDouble(value){
		var valueValid = parseFloat(value);
		var regExp = /^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/;
		
		if(value == ""){
			showToast('warning','Please enter value');	
			return false;
		} else if (!regExp.test(value)) { 		
			showToast( 'warning','Please enter valid price value in the format of floating point value');	
			return false;
		} else if (valueValid == '0' || valueValid == '0.0') { 		
			showToast( 'warning','Please enter value more than zero (0)');
			return false;
		}
		
		return true;
	}

	
	function validate()
	{
		
		var lat;
		var lng;
		lat = $('#serviceCentreLatitude').val().trim();
		if(!checkOfValidDouble(lat)){
			$('#serviceCentreLatitude').focus();
			return false;
		}
		
		lng=$('#serviceCentreLongitude').val().trim();
		if(!checkOfValidDouble(lng))
			{
				$('#serviceCentreLongitude').focus();
				return false;
			}
		
		
		
		var serviceCentre = $('#serviceCentreName').val().trim(), chardig= /^[a-zA-Z ]*$/;
		
		
		
		if(!chardig.test(serviceCentre))
			{
			showToast('warning','Please enter Valid ServiceCentre in Characters');
			return false;
			}
		else if(!serviceCentre)
			{
			showToast('warning','Please enter Valid ServiceCentre');
			return false;
			}
		return true;
		
	}

</script>
</head>
<c:url value="/admin/serviceCentre/add" var="serviceCentreAdd" />
<c:url value="/admin/serviceCentre/list" var="serviceCentreList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/serviceCentre/add" var="addServiceCentre" />

	<form:form id="addServiceCentre" action="${serviceCentreAdd}" method="post"
		modelAttribute="serviceCentre" enctype="application/x-www-form-urlencoded">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">ServiceCentre </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
			<fieldset>
			
			
				
		

			<form:errors path="serviceCentreName" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name *</span>
				<form:input path="serviceCentreName" id ="serviceCentreName" cssClass="form-control" />
			</div>
			<form:errors path="serviceCentreAddress" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Address *</span>
				<form:input path="serviceCentreAddress" id ="serviceCentreAddress" cssClass="form-control" />
			</div>
		
			<form:errors path="city"  cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> City *</span>
		<form:select path="city" id="city" name="city" class="form-control">
		<option value="-1" >-- Select city--</option>
		<c:forEach items="${cities}" var="city">
		<form:option value="${city.id }"> ${city.name}</form:option>
		</c:forEach>
		
			
				
			</form:select>
			</div>
	
				<form:errors path="serviceCentrePostalCode" cssClass="text-danger" />
			<div class="input-group width-xlarge">   
				<span class="input-group-addon"> Postal code *</span>
				<form:input path="serviceCentrePostalCode" id ="serviceCentrePostalCode" cssClass="form-control" />
				</div>
				<form:errors path="serviceCentreLatitude" cssClass="text-danger" />
			<div class="input-group width-xlarge">    
			<span class="input-group-addon"> Latitude *</span>
			<form:input path="serviceCentreLatitude" id ="serviceCentreLatitude" onchange="validationOfLatitude()" cssClass="form-control" />
			</div>
			
			<form:errors path="serviceCentreLongitude" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				
				<span class="input-group-addon"> Longitude *</span>
				<form:input path="serviceCentreLongitude" id ="serviceCentreLongitude"  onchange="validationOfLongitude()" cssClass="form-control" />
			</div>
			<form:errors path="serviceCentreStartTime" cssClass="text-danger" />
			<div class="input-group width-xlarge">
			<span class="input-group-addon">Start Time *</span>
				<form:input path="serviceCentreStartTime" data-provide="timepicker" data-minute-step="1" maxlength="12" id ="serviceCentreStartTime" cssClass="form-control" />
			
			</div>
			<form:errors path="serviceCentreEndTime" cssClass="text-danger" />
			<div class="input-group width-xlarge">
<span class="input-group-addon">End Time *</span>
				<form:input path="serviceCentreEndTime" data-provide="timepicker" data-minute-step="1" maxlength="12" id ="serviceCentreEndTime" cssClass="form-control" />
			
			</div>
		</fieldset>
		</div>	
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:submitForm()">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>