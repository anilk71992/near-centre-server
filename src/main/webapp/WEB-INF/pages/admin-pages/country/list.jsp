<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Country</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Country</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Country</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/country/list" />
	<c:set var="urlPrefix" value="/admin/country/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/country/show" var="countryDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="countries" class="footable" requestURI="" id="country" export="false">
			<display:column title="Code" sortable="true">
				<a href="${countryDetailLinkPrefix}/${country.cipher}">${country.code}</a>
			</display:column>
			
			<display:column title="Name" sortable="true">${country.name}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/country/update/${country.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/country/delete/${country.cipher}">
							<li><a href="#ConfirmModal_${country.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${country.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Country?</h3>
							</div>
							<div class="modal-body">
								<p>The Country will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/country/delete/${country.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

</body>
</html>