<%@ include file="/taglibs.jsp"%>

<html>
<head>
<title>NearCentre - Message Send</title>
</head>
<body>
	<h1>Contact List</h1>
	<br />
	<label class="col-md-6">Total number of Contact Message Send :-${mobileNumberLists.size()}</label><br/>
	<div class="panel panel-info" id="primary-info">
		<div class="panel-heading clearfix"></div>
		<div class="panel-body">
				<c:forEach items="${mobileNumberLists}" var="mobileNumber">
				<div class="row">
					<label class="col-md-2">Mobile Number: ${mobileNumberLists.indexOf(mobileNumber)}</label>
					<p class="col-md-2">${mobileNumber}</p>
				</div>
				</c:forEach>

		</div>
	</div>
	<span class="label label-warning customWarning">This Page For Only Confirmation </span><br/><br>
	<a class="btn btn-danger" href="javascript:history.back(1)">Back </a>
</body>
</html>
