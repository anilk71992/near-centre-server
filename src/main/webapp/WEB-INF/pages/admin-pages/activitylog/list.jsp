<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Activity Log</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Activity Log</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Activity Log</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/activityLog/list" />
	<c:set var="urlPrefix" value="/admin/activityLog/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/activityLog/show" var="activityLogDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="activityLogs" class="footable" requestURI="" id="activityLog" export="false">
			<display:column title="Code" sortable="true">
				<a href="${activityLogDetailLinkPrefix}/${activityLog.cipher}">${activityLog.code}</a>
			</display:column>
			
			<display:column title="User Name" sortable="true">${activityLog.user.firstName} ${activityLog.user.lastName}-${activityLog.user.mobileNumber}</display:column>
			<display:column title="Activity" sortable="true">${activityLog.activity}</display:column>
			<display:column title="Entity Type" sortable="true">${activityLog.entityType}</display:column>
			<display:column title="Entity Id" sortable="true">${activityLog.entityId}</display:column>
		
		</display:table>
	</form>


</body>
</html>