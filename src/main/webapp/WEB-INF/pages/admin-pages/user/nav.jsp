<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/user/addUser" var="userAdd" />
<c:url value="/admin/user/list" var="userList" />
<c:url value="/admin/user/import" var="userImport"/>
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<a class="btn btn-primary" href="${userAdd}">Add New User</a>&nbsp;
	<%-- <a class="btn btn-primary" href="${userImport}">Import Users</a>&nbsp;
	 --%><form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>