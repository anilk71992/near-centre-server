<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>NearCentre - Reports</title>
<script type="text/javascript">
$(document).ready(function(){
	$('#date').datepicker({autoclose:true});
});
</script>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
<!-- 	<div class="row"> -->
<!-- 		<div class="col-lg-12"> -->
<!-- 			<h3 class="page-header">Reports</h3> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<form:form action="${reportsList}" class="form-inline">

		<fieldset>
			<h3 class="page-header">Reports</h3>
		<sec:authorize access="!hasAnyAuthority('ROLE_INTERN','ROLE_CCE')">
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Date</span>
				<input name="date" id="date" class="form-control" data-date-format="dd-mm-yyyy" required  />
			</div>
			<button class="btn btn-info" type="submit">Search</button>&nbsp;&nbsp;&nbsp;&nbsp;
			<br>
		</sec:authorize>
			
		</fieldset>
	</form:form>
	<br>
	<c:if test="${not empty date}">
		<h5><i>Reports for date ${date}</i></h5>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Product</th>
					<th>Quantity</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${products}" var="product">					
					<tr>
						<td>${product.key.name}</td>
						<td>${product.value}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<th>Total</th>
					<th>${totalQuntity}</th>
				</tr>
				<tr></tr>
			</tfoot>
		</table>
	</c:if>
</body>
</html>