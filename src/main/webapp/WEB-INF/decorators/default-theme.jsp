<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><decorator:title default="Welcome" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Inspinia Theme integration CSS add start-->
<link href="<c:url value="/assets/inspinia/css/bootstrap.min.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/css/plugins/toastr/toastr.min.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/js/plugins/gritter/jquery.gritter.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/css/animate.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/css/style.css"/>" rel="stylesheet">
<!-- Inspinia theme Integration CSS add complete -->

<link href="<c:url value="/assets/css/customwidths.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/css/site.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/css/datepicker.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/css/select2.min.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/css/fullcalendar.css"/>" rel="stylesheet">

<script src="<c:url value="/assets/js/jquery-2.2.1.min.js"/>"></script>

<!-- Inspinia theme integration JS add start -->
<script src="<c:url value="/assets/inspinia/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
<!-- Flot -->
<script src="<c:url value="/assets/inspinia/js/plugins/flot/jquery.flot.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/flot/jquery.flot.spline.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/flot/jquery.flot.resize.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/flot/jquery.flot.pie.js"/>"></script>
<!-- Peity -->
<script src="<c:url value="/assets/inspinia/js/plugins/peity/jquery.peity.min.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/demo/peity-demo.js"/>"></script>
<!-- Custom and plugin javascript -->
<script src="<c:url value="/assets/inspinia/js/inspinia.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/plugins/pace/pace.min.js"/>"></script>
<!-- jQuery UI -->
<script src="<c:url value="/assets/inspinia/js/plugins/jquery-ui/jquery-ui.min.js"/>"></script>
<!-- GITTER -->
<script src="<c:url value="/assets/inspinia/js/plugins/gritter/jquery.gritter.min.js"/>"></script>
<!-- Sparkline -->
<script src="<c:url value="/assets/inspinia/js/plugins/sparkline/jquery.sparkline.min.js"/>"></script>
<!-- Sparkline demo data  -->
<script src="<c:url value="/assets/inspinia/js/demo/sparkline-demo.js"/>"></script>
<!-- ChartJS-->
<script src="<c:url value="/assets/inspinia/js/plugins/chartJs/Chart.min.js"/>"></script>
<!-- Inspinia theme integration JS add End -->

<script src="<c:url value="/assets/js/underscore-min.js"/>"></script>
<script src="<c:url value="/assets/js/bootstrap-datepicker.js"/>"></script>
<script src="<c:url value="/assets/js/select2.min.js"/>"></script>
<script src="<c:url value="/assets/js/fullcalendar.js"/>"></script>
<script src="<c:url value="/assets/js/moment.js"/>"></script>
<script src="<c:url value="/assets/end-user/js/toastr.min.js"/>"></script>
<script src="<c:url value="/assets/js/jscolor.js"/>"></script>
<script src="<c:url value="/assets/js/jquery.checkboxes-1.0.6.min.js"/>"></script>

<link href="<c:url value="/assets/css/footable.standalone.css"/>" rel="stylesheet">
<script src="<c:url value="/assets/js/footable.js"/>"></script>
<script type="text/javascript">
	
</script>

<script type="text/javascript">
_.templateSettings = {
	    interpolate: /\<\@\=(.+?)\@\>/gim,
	    evaluate: /\<\@(.+?)\@\>/gim,
	    escape: /\<\@\-(.+?)\@\>/gim,
	    variable: "rc"
	};
function hideModel(id) {
	$("#"+id).toggle('hide');
}

function showToast(type,message){
	Command: toastr[type](message);

	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": false,
	  "positionClass": "toast-top-center",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
}
//it is used to initialse footable
$(function () {
	$('.footable').footable();
});

</script>

<decorator:head />
</head>

<body>
	<c:url value="/home" var="homeUrl" />
	<c:url value="/login" var="loginUrl" />
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/admin/access/changePassword" var="changePasswordUrl" />
	<c:url value="/admin/user/list" var="userListUrl"/>
	<c:url value="/admin/user/getCustomerlist" var="customerListUrl"/>
	<c:url value="/admin/user/getNewlyCustomerlist" var="getNewlyCustomerListUrl"/>
	
	<c:url value="/admin/route/list" var="routeListUrl"/>
	<c:url value="/admin/product/list" var="productListUrl"/>
	<c:url value="/admin/routeRecord/list" var="routeRecordListUrl"/>
	<c:url value="/admin/userRoute/list" var="userRouteListUrl"/>
	<c:url value="/admin/userSubscription/list" var="userSubscriptionListUrl"/>
	<c:url value="/admin/subscriptionLineItem/list" var="subscriptionLineItemListUrl"/>
	<c:url value="/admin/category/list" var="categoryListUrl"/>
	<c:url value="/admin/payment/list" var="paymentListUrl"/>
	<c:url value="/admin/invoice/list" var="invoiceListUrl"/>
	<c:url value="/admin/deliverySchedule/showCalender" var="deliveryScheduleListUrl"/>
	<c:url value="/admin/deliveryLineItem/list" var="deliveryLineItemListUrl"/>
	<c:url value="/admin/undeliveredOrder/list" var="undeliveredOrderListUrl"/>
	<c:url value="/admin/bouncedPayment/list" var="bouncedPaymentListUrl"/>
	<c:url value="/admin/brand/list" var="brandListUrl" />
	<c:url value="/admin/product/list" var="productListUrl"/>
	<c:url value="/admin/serviceCentre/list" var="serviceCentreListUrl"/>
	<c:url value="/admin/user/show" var="deliveryBoysShowUrl"/>
	<c:url value="/admin/summarySheet/list" var="summarySheetListUrl"/>
	<c:url value="/admin/deliverySheet/list" var="deliverySheetUrl"/>
	<c:url value="/admin/deliverySchedule/list" var="deliveryScheduleUrl"/>
	<c:url value="/admin/reports/list" var="reportsListUrl"/>
    <c:url value="/admin/systemProperty/list" var="systemPropertyListUrl"/>
    <c:url value="/admin/payment/details" var="paymentDetailsListUrl"/>
    <c:url value="/admin/deliverySchedule/newCustomerCalendar" var="newCustomerCalendar"/>
    <c:url value="/admin/deliverySchedule/stopCustomerCalendar" var="stopCustomerCalendar"/>
    <c:url value="/admin/user/pendingDuesList" var="pendingDuesList"/>
    <c:url value="/admin/deposit/accountProfile" var="collectionBoyProfileUrl" />
    <c:url value="/admin/country/list" var="countryListUrl"/>
	<c:url value="/admin/city/list" var="cityListUrl"/>
	<c:url value="/admin/locality/list" var="localityListUrl"/>
	<c:url value="/admin/address/list" var="addressListUrl"/>
	<c:url value="/admin/weimage/show" var="weImageShow"/>
	<c:url value="/admin/payment/unVerifiedChequelist" var="unVerifiedChequeListUrl"/>
	<c:url value="/admin/payment/undepositedChequelist" var="unDepositedChequeListUrl"/>
	<c:url value="/admin/onetimeorder/list" var="oneTimeOrderUrl"/>
	<c:url value="/admin/onetimeorder/todaylist" var="oneTimeOrderUrltodaylist"/>
	<c:url value="/admin/activityLog/list" var="activityLogListUrl" />
	<c:url value="/admin/subscriptionRequest/list" var="subscriptionRequestListUrl" />
	<c:url value="/admin/message/sendMessage" var="sendMessageUrl" />
	<c:url value="/admin/user/currentCustomer/export/${'ACTIVE'}" var="activeExport" />
	<div id="wrapper">
		
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
            	<sec:authorize access="isAuthenticated()">
	                <ul class="nav metismenu" id="side-menu">
	                    <li class="nav-header">
								<h2 style="color: #9FA9BE">NearCentre</h2>
	                    </li>
	                    <li class="active">
	                        <a href="${homeUrl}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>                        
	                    </li>
	                    <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_INTERN','ROLE_CCE','ROLE_ACCOUNTANT')">
	                    <li>
	                        <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	<sec:authorize  access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
									<li><a href="${userListUrl}">Users</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${customerListUrl}">Customers</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${getNewlyCustomerListUrl}">New Customers</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${newCustomerCalendar}">New Customer Calendar</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${stopCustomerCalendar}">Stop Customer Calendar</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${pendingDuesList}">Pending Dues List</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li class="active"><a id="domain" href="${pendingDuesList}">Current Customer<span class="fa arrow"></span></a>
										<ul class="nav nav-third-level collapse in">
										  <li><a href="${activeExport}">Active</a></li>
										  <li><a href="${inactiveExport}">Inactive</a></li>
										  <li><a href="${holdExport}">Hold</a></li>
										  <li><a href="${newExport}">New</a></li>
										</ul>
									</li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${customerStatusReport}">Customer Status Report</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	             
	                    <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_INTERN','ROLE_CCE')">
		                    <li>
		                        <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Addresses</span><span class="fa arrow"></span></a>
		                        <ul class="nav nav-second-level collapse">
		                        	 <sec:authorize access="isAuthenticated()">
			                   			 <li>
			                        		<a href="${countryListUrl}"><i class="fa fa-globe"></i> <span class="nav-label">Countries</span>  </a>
			                   			 </li>
									</sec:authorize>
		                        </ul>
		                        <ul class="nav nav-second-level collapse">
		                        	 <sec:authorize access="isAuthenticated()">
					                    <li>
					                        <a href="${cityListUrl}"><i class="fa fa-map-marker"></i> <span class="nav-label">Cities</span>  </a>
					                    </li>
									</sec:authorize>
		                        </ul>
		                   
		                        <%--  <ul class="nav nav-second-level collapse">
		                        	 <sec:authorize access="isAuthenticated()">
					                    <li>
					                        <a href="${localityListUrl}"><i class="fa fa-map-marker"></i> <span class="nav-label">Localities</span>  </a>
					                    </li>
									</sec:authorize>
		                        </ul> --%>
		                         <ul class="nav nav-second-level collapse">
		                        	<sec:authorize access="isAuthenticated()">
					                    <li>
					                        <a href="${addressListUrl}"><i class="fa fa-location-arrow"></i> <span class="nav-label">Addresses</span>  </a>
					                    </li>
									</sec:authorize>
		                        </ul>
		                    </li>
	                    </sec:authorize>
						
	                     <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
	                    <li>
	                        <a href="#"><i class="fa fa-code-fork"></i> <span class="nav-label">Routes</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	<sec:authorize access="isAuthenticated()">
									<li><a href="${routeListUrl}">Routes</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${routeRecordListUrl}">Route Records</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${userRouteListUrl}">User Routes</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	               
	                    <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
		                    <li>
		                        <a href="${weImageShow}"><i class="fa fa-camera"></i> <span class="nav-label">Images</span>  </a>
		                    </li>
						</sec:authorize>
						 <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
		                  
		                   <li>
	                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">One Time Order</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	<sec:authorize access="isAuthenticated()">
									<li><a href="${oneTimeOrderUrltodaylist}">Todays Orders</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${oneTimeOrderUrl}">All Orders</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
						</sec:authorize>
	                    	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CCE')">
	                    <li>
	                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Products</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	
								<sec:authorize access="isAuthenticated()">
									<li><a href="${productListUrl}">Products</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	                   	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CCE')">
	                    <li>
	                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Brands</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	
								<sec:authorize access="isAuthenticated()">
									<li><a href="${brandListUrl}">Brands</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	                    
	                      	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CCE')">
	                    <li>
	                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Service Centre</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	
								<sec:authorize access="isAuthenticated()">
									<li><a href="${serviceCentreListUrl}">Service Centre</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	                    
	                    <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_INTERN','ROLE_CCE')">
		                    <li>
		                        <a href="${userSubscriptionListUrl}"><i class="fa fa-key"></i> <span class="nav-label">User Subscriptions</span>  </a>
		                    </li>
		               </sec:authorize>
		               
		                <li>
	                        <a href="#"><i class="fa fa-gears"></i> <span class="nav-label">Subscription Requests</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
		                <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_INTERN','ROLE_CCE')">
		                    <li>
		                        <a href="${subscriptionRequestListUrl}"><i class="fa fa-key"></i> <span class="nav-label">New</span>  </a>
		                    </li>
		                     <li>
		                        <a href="${changeSubscriptionRequestListUrl}"><i class="fa fa-key"></i> <span class="nav-label">Change</span>  </a>
		                    </li>
		               </sec:authorize>
		               </ul>
		               </li>
							
						<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CCE','ROLE_ACCOUNTANT')">	              
		             	 <li>
	                        <a href="#"><i class="fa fa-gears"></i> <span class="nav-label">Delivery Schedules</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	 <sec:authorize access="isAuthenticated()">
									<li><a href="${deliveryScheduleListUrl}">Calendar</a></li>
									</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${deliveryScheduleUrl}">Trial Delivery Schedules</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
		               <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT','ROLE_CCE')">
		                    <li>
		                        <a href="${invoiceListUrl}"><i class="fa fa-print"></i> <span class="nav-label">Invoices</span>  </a>
		                    </li>
		               </sec:authorize>
		               
		                 <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CCE','ROLE_ACCOUNTANT')">
	                    <li>
	                        <a href="#"><i class="fa fa-money"></i> <span class="nav-label">Payments</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	<sec:authorize access="isAuthenticated()">
									<li><a href="${paymentListUrl}">Payments</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${unDepositedChequeListUrl}">UnDeposited Cheques</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
								<li><a href="${unVerifiedChequeListUrl}">UnVerified Cheques</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
		                <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
		               <li>
	                        <a href="#"><i class="fa fa-eye"></i> <span class="nav-label">Sheets</span><span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse">
	                        	<sec:authorize access="isAuthenticated()">
									<li><a href="${summarySheetListUrl}">Summary Sheets</a></li>
								</sec:authorize>
								<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
									<li><a href="${deliverySheetUrl}">Delivery Sheets</a></li>
								</sec:authorize>
								<sec:authorize access="isAuthenticated()">
									<li><a href="${collectionBoyProfileUrl}">CollectionBoy Profile</a></li>
								</sec:authorize>
	                        </ul>
	                    </li>
	                    </sec:authorize>
	                     <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
		                    <li>
		                        <a href="${reportsListUrl}"><i class="fa fa-envelope-o"></i> <span class="nav-label">Reports</span></a>
		                    </li>
		               </sec:authorize>
		                 <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN')">
		                    <li>
		                        <a href="${systemPropertyListUrl}"><i class="fa fa-list-alt"></i> <span class="nav-label">System Property</span></a>
		                    </li>
		               </sec:authorize> 
		                 <sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
		                    <li>
		                        <a href="${paymentDetailsListUrl}"><i class="fa fa-money"></i> <span class="nav-label">Payment Details</span></a>
		                    </li>
		               </sec:authorize> 
						<sec:authorize access="isAuthenticated()">
		                    <li>
		                        <a href="${activityLogListUrl}"><i class="fa fa-cog"></i> <span class="nav-label">Activity Logs</span>  </a>
		                    </li>
						</sec:authorize> 
						<sec:authorize access="isAuthenticated()">
		                    <li>
		                        <a href="${sendMessageUrl}"><i class="fa fa-envelope"></i> <span class="nav-label">Send Message</span>  </a>
		                    </li>
						</sec:authorize> 
						<sec:authorize access="isAuthenticated()">
		                    <li>
		                        <a href="${sendByRoute}"><i class="fa fa-mobile"></i> <span class="nav-label">Push & SMS & Email</span>  </a>
		                    </li>
						</sec:authorize> 
	                </ul>
                </sec:authorize>
            </div>
        </nav>
        
	
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
			            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			        </div>
					<ul class="nav navbar-top-links navbar-right">
						<sec:authorize access="isAnonymous()">
							<li>
			                    <a href="${loginUrl}"> <i class="fa fa-sign-out"></i> Login</a>
			                </li>
			           </sec:authorize>
						<sec:authorize access="isAuthenticated()">
							<li>
			                    <a href="${logoutUrl}"> <i class="fa fa-sign-out"></i> Log out</a>
			                </li>
						</sec:authorize>
						<sec:authorize access="isAuthenticated()">
							<li>
			                    <a href="${changePasswordUrl}"> <i class="fa fa-sign-out"></i> Change Password</a>
			                </li>
						</sec:authorize>
					</ul>
				</nav>
			</div>
			<div class="row  border-bottom white-bg dashboard-header">
					<%@ include file="/messages.jsp"%>
					<decorator:body />
					<br>
			</div>
		</div>
	</div>

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->


</body>
</html>v<%@ include file="/taglibs.jsp"%>
