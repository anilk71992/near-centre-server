<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><decorator:title default="Engazify" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Core CSS -->
<link href="<c:url value="/assets/end-user/css/bootstrap.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/assets/end-user/css/animate.css"/>" rel="stylesheet" />
<link href="<c:url value="/assets/end-user/css/style.css"/>" rel="stylesheet" />
<link href="<c:url value="/assets/end-user/css/stylesheet.css"/>" rel="stylesheet"/>
<!-- End Core CSS -->

<!-- Toastr CSS -->
<link href="<c:url value="/assets/end-user/css/toastr.min.css"/>" rel="stylesheet" />
<!-- End Toastr CSS -->

<!-- Gritter CSS-->
<link href="<c:url value="/assets/end-user/css/jquery.gritter.css"/>" rel="stylesheet" />
<!-- Gritter CSS -->

<!-- Select 2 CSS -->
<link href="<c:url value="/assets/end-user/css/select2.min.css"/>" rel="stylesheet"/>
<link href="<c:url value="/assets/end-user/css/select2.css"/>" rel="stylesheet"/>
<link href="<c:url value="/assets/end-user/css/select2-bootstrap.css"/>" rel="stylesheet"/>
<!-- End Select 2 CSS -->

<!-- Font Awesome -->
<link href="<c:url value="/assets/end-user/css/font-awesome.css"/>" rel="stylesheet" />
<link href="<c:url value="/assets/end-user/css/font-awesome-latest.css"/>" rel="stylesheet" />
<!-- End Font Awesome -->
      
<!-- Selectize CSS -->
<link href="<c:url value="/assets/end-user/css/selectize.default.css"/>" rel="stylesheet"/>
<!-- End Selectize CSS -->

<!-- Custom Scrollbar CSS -->
<link href="<c:url value="/assets/end-user/css/jquery.mCustomScrollbar.min.css"/>" rel="stylesheet"/>
<!-- End Custom Scrollbar CSS -->

<!-- Post-box -->
<link href="<c:url value="/assets/end-user/css/post-box.css"/>" rel="stylesheet"/>
<!-- End Post-box -->

<!-- Fancy-box -->
<link href="<c:url value="/assets/end-user/css/jquery.fancybox.css"/>" rel="stylesheet"/>
<!-- End Fancy-box -->

<!-- Bootstrap Checkbox -->
<link href="<c:url value="/assets/end-user/css/awesome-bootstrap-checkbox.css"/>" rel="stylesheet"/>
<!-- End Bootstrap Checkbox -->

<!-- iCheck  -->
<link href="<c:url value="/assets/end-user/css/polaris.css"/>" rel="stylesheet" />
<link href="<c:url value="/assets/end-user/css/icheck-green.css"/>" rel="stylesheet" />
<!-- End iCheck -->

<!-- Mainly scripts -->
<script src="<c:url value="/assets/end-user/js/jquery-2.1.1.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/bootstrap.min.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.metisMenu.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.slimscroll.min.js"/>"></script> 
         
<!-- Flot --> 
<script src="<c:url value="/assets/end-user/js/jquery.flot.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.flot.tooltip.min.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.flot.spline.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.flot.resize.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.flot.pie.js"/>"></script> 
         
<!-- Peity --> 
<script src="<c:url value="/assets/end-user/js/jquery.peity.min.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/peity-demo.js"/>"></script> 
         
<!-- Custom and plugin javascript --> 
<script src="<c:url value="/assets/end-user/js/inspinia.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/pace.min.js"/>"></script> 
         
<!-- jQuery UI --> 
<script src="<c:url value="/assets/end-user/js/jquery-ui.min.js"/>"></script> 
         
<!-- GITTER --> 
<script src="<c:url value="/assets/end-user/js/jquery.gritter.min.js"/>"></script> 
         
<!-- Sparkline --> 
<script src="<c:url value="/assets/end-user/js/jquery.sparkline.min.js"/>"></script> 
         
<!-- Sparkline demo data  -->
<script src="<c:url value="/assets/end-user/js/sparkline-demo.js"/>"></script> 

<!-- ChartJS--> 
<script src="<c:url value="/assets/end-user/js/Chart.min.js"/>"></script> 
         
<!-- Toastr -->
<script src="<c:url value="/assets/end-user/js/toastr.min.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/jquery.mCustomScrollbar.concat.min.js"/>"></script> 
         
<!-- Maximize--> 
<script src="<c:url value="/assets/end-user/js/site.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/select2.full.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/selectize.js"/>"></script> 
<script src="<c:url value="/assets/end-user/js/app.js"/>"></script> 
         
<!-- iCheck -->
<script src="<c:url value="/assets/end-user/js/icheck.js"/>"></script> 
         
<!-- Post box --> 
<script src="<c:url value="/assets/end-user/js/post-box.js"/>"></script>

<!-- Fancy box --> 
<script src="<c:url value="/assets/end-user/js/jquery.fancybox.pack.js?v=2.1.5"/>"></script> 
<script src="<c:url value = "/assets/end-user/js/cropper/cropper.min.js"/>"></script>            
<script type="text/javascript">

	var membersToRecognise = new Array();
	var memberIdsToRecognise = new Array();
	<c:forEach items="${membersToRecognise}" var="member">
		var memberObj = new Object();
		memberObj['firstName'] = '${member.firstName}';
		memberObj['lastName'] = '${member.lastName}';
		memberObj['email'] = '${member.email}';
		memberObj['id'] = '${member.id}';
		membersToRecognise.push(memberObj);
		memberIdsToRecognise.push('${member.id}');
	</c:forEach>

    $(document).ready(function() {
    	
    	$('#input-recognize, #input-tags').selectize({
            closeAfterSelect: true,
            openOnFocus: false,
            dropdownParent: "body",
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'email',
            searchField: 'email',
            create: false,
            options: membersToRecognise,
            items: membersToRecognise,
            render: {
                item: function(data, escape) {
                    console.log(data);
                    return '<div>' +
                        '<span class="name"><i class="icon"></i>' + escape(data.firstName) + escape(data.lastName) + '</span>' +
                        '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name"><i class="icon"></i>' + escape(item.firstName) + escape(item.lastName) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: '<c:url value="/restapi/member/search"/>',
                    type: 'GET',
                    dataType: 'json',
                    data: 'searchTerm=' + query,
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                    	if(res.success) {
                    		callback(res.data.members);
                    	} else {
                    		callback();
                    	}
                    }
                });
            }
        });
    	
	    $(".fancybox, .fancybox-post").fancybox({
	        openEffect  : 'none',
	        closeEffect : 'none',
	        arrows: false
	    });
	});
</script>

 <script type="text/javascript">

        var $imageInput = $('#inputImage');
        var $image = $('#imagepreview');
        var imageData;

        $(document).ready(function(){

            var cropBoxData;
            var canvasData;

            $('#myModal5').on('shown.bs.modal', function () {
                $image.cropper({
                    autoCropArea: 0.8,
                    built: function () {
                        $image.cropper('setCanvasData', canvasData);
                        $image.cropper('setCropBoxData', cropBoxData);
                        $image.cropper('replace', imageData);
                        $image.cropper('setAspectRatio', 1);
                        $image.cropper('setCropBoxResizable', false);
                        $image.cropper('setMode', 'move');
                    }  
                });
            }).on('hidden.bs.modal', function () {
                cropBoxData = $image.cropper('getCropBoxData');
                canvasData = $image.cropper('getCanvasData');
                $image.cropper('destroy');
            });

            $imageInput.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $imageInput.val("");
                        imageData = this.result;
                        $('#myModal5').modal('show');
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
        $(".fancybox, .fancybox-post").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            arrows: false
        });
    });
    </script>
    <!--End -->
    
<!-- img modal -->
<script type="text/javascript">
    $(function () {
        $('.pop').on('click', function () {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
    });
</script>

<!-- <script type="text/javascript">
_.templateSettings = {
	    interpolate: /\<\@\=(.+?)\@\>/gim,
	    evaluate: /\<\@(.+?)\@\>/gim,
	    escape: /\<\@\-(.+?)\@\>/gim,
	    variable: "rc"
	};
</script> -->
<decorator:head />
</head>

<body>
	<c:url value="/home" var="homeUrl" />
	<c:url value="/login" var="loginUrl" />
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/company/list" var="companyListUrl" />
	<c:url value="/member/list" var="memberListUrl" />
	<c:url value="/award/list" var="awardListUrl" />
	<c:url value="/recognition/list" var="recognitionListUrl" />
	<c:url value="/companyBasePlan/list" var="companyBasePlanListUrl" />
	<c:url value="/companyPlan/list" var="companyPlanListUrl" />
	<c:url value="/post/list" var="postListUrl" />
	<c:url value="/giftCategory/list" var="giftCategoryListUrl" />
	<c:url value="/giftCard/list" var="giftCardListUrl" />
	<c:url value="/redemption/list" var="redemptionListUrl" />
	<c:url value="/event/list" var="eventListUrl" />
	
	<c:url value="/member/add" var="addMemberUrl"/>
	<c:url value="/member/addEvent" var="createEventUrl"/>
	<c:url value="/member/profile" var="memberProfileUrl"/>
	<c:url value="/connections" var="connectionsUrl"/>
	<c:url value="/myTeam" var="myTeamUrl"/>
	<div id="wrapper">
		<!-- Left Nav bar -->
		<sec:authorize access="isAuthenticated()">
			<sec:authentication property="principal" var="loggedInUser" />
			<nav class="navbar-default navbar-static-side" role="navigation" style="margin-top:60px">
		        <div class="sidebar-collapse">
		            <ul class="nav metismenu" id="side-menu">
		                <li class="nav-header">
		                    <div class="asideUserProfile ">
		                        <a class="fancybox-post" rel="group" href="">
		                            <div class="profile-img-container">
		                                <img src="<c:url value="/restapi/getProfilePicture?id=${loggedInUser.id}"/>" alt="#" class="img-responsive profile-image">
		                                <i class="fa fa-camera fa-2x pick-photo btn" type="file" id="inputImage"></i>
		                            </div>
		                        </a>

							</div>
		                    <div class="stats-label text-color">
		                        <div class="profile-details">
		                            <p class="font-extra-bold font-uppercase" style="margin: 0 12px 8px;">${loggedInUser.firstName} ${loggedInUser.lastName}</p>
		
		                        </div>
		
		
		                        <div>
		                            <h3 class="font-extra-bold m-b-xs points-number">
		                       
		                            </h3>
		
		                            <h3 class="text-muted points-text">Points Balance</h3>
		                        </div>
		
		                        <div id="rcorners1">
		                            <h3 class="text-muted points-award-text">Points to Award</h3>
		
		                            <h3 class="font-extra-bold m-b-xs points-award-number">
		                                
		                            </h3>
		                        </div>
		                    </div>
		                </li>
		
		                <li class="active">
		                    <a href="${homeUrl}"><i class="fa fa-home"></i> <span class="nav-label">Home</span> </a>
		                </li>
		                <li>
		                    <a href="profile.html"><i class="fa fa-user"></i> <span class="nav-label">Profile</span> <span
		                            class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li><a href="${memberProfileUrl}">About</a></li>
		                        <li><a href="dashboard_2.html">My Wall</a></li>
		                        <li><a href="dashboard_3.html">Awards Received</a></li>
		                        <li><a href="dashboard_4_1.html">Awards Given</a></li>
		                        <li><a href="dashboard_5.html">Microevents</a></li>
		                        <li><a href="dashboard_5.html">Orders</a></li>
		                    </ul>
		                </li>
		
		                <li>
		                    <a href="${homeUrl}"><i class="fa fa-bookmark"></i> <span class="nav-label">Recognize</span></a>
		
		                </li>
		                <li>
		                    <a href="Shop.html"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Shop </span></a>
		
		                </li>
		                <li>
		                    <a href="${connectionsUrl}"><i class="fa fa-users"></i> <span class="nav-label">Connections</span> </a>
		                </li>
		                <li>
		                    <a href="${myTeamUrl}"><i class="fa fa-sitemap"></i> <span class="nav-label">My Team</span></a>
		                </li>
		                <li>
		                    <a href="Admin-reports.html"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reports</span><span
		                            class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level collapse">
		                        <li><a href="Admin-reports.html">Awards</a></li>
		                        <li><a href="form_advanced.html">Awards by Type</a></li>
		                        <li><a href="form_wizard.html">Budget Usage</a></li>
		                        <li><a href="form_file_upload.html">Microevents</a></li>
		                        <li><a href="form_editors.html">Daily Happenings</a></li>
		                    </ul>
		                </li>
		                <li>
		                    <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Admin</span> <span
		                            class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level collapse">
		                        <li><a href="${addMemberUrl}">Employees</a></li>
		                        <li><a href="${createEventUrl}">Events</a></li>
		                         <li><a href="Admin-reports.html">Reports</a></li>
		
		                    </ul>
		                </li>
		
		                <li>
		                    <a href="metrics.html"><i class="fa fa-question"></i> <span class="nav-label">FAQ's</span></a>
		                </li>
		                <li>
		                    <a href="metrics.html"><i class="fa fa-envelope"></i> <span class="nav-label">Contact Us</span> </a>
		                </li>
		                <li>
		                    <a href="metrics.html"><i class="fa fa-file-text"></i> <span class="nav-label">T & C</span> </a>
		                </li>
		
		        </div>
		    </nav>
        </sec:authorize>
        <!-- End Left Nav bar -->
        <div id="page-wrapper" class="gray-bg dashbard-1">
	        <!-- Top Nav Bar + iBox Status -->
	        <sec:authorize access="isAuthenticated()">
		        <div class="row border-bottom white-bg dashboard-header" id="page-bg">
		        	<!-- Top Nav Bar -->
		            <div class="row">
		                <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
							<div class="navbar-header">
								<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
									class="fa fa-bars"></i> </a>
								<div class="logo">
									<a href="${homeUrl}" class="logo-expanded">
									<img src="<c:url value="/assets/end-user/images/img/Engazify Logo.png"/>" alt="logo">
									<span>engazify</span>
									</a>
								</div>
							</div>
							<ul class="nav navbar-top-links navbar-right">
								<li class="dropdown">
									<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" style="top: 6px;">
									<i class="fa fa-bell"></i> <span class="label label-primary">8</span>
									</a>
									<ul class="dropdown-menu dropdown-alerts scrollable-menu" id="ex4">
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Bruce Welch.jpg"/>">
												</a>
												<div class="media-body">
													<small class="pull-right">46h ago</small>
													<strong>Bruce Welch</strong> liked your post 
													<strong> Recognized as a star</strong>. <br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Cheryl Gordon.jpg"/>">
												</a>
												<div class="media-body ">
													<small class="pull-right">5h ago</small>
													<strong>Cheryl Gordon</strong> gave you a <strong> Thank You Award</strong>.
													<br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Jeffrey Torres.jpg"/>">
												</a>
												<div class="media-body ">
													<small class="pull-right">5h ago</small>
													<strong>Jeffrey Torres</strong> gave you a <strong> Thank You Award</strong>.
													<br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Matthew Sanders.jpg"/>">
												</a>
												<div class="media-body">
													<small class="pull-right">46h ago</small>
													<strong>Matthew Sanders</strong> liked your post <strong> Recognized as a
													star</strong>. <br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Debra Burton.jpg"/>">
												</a>
												<div class="media-body ">
													<small class="pull-right">5h ago</small>
													<strong>Debra Burton</strong> gave you a <strong> Thank You Award</strong>.
													<br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
										<li>
											<div class="dropdown-messages-box" style="padding-bottom: 5px;">
												<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" style="border-radius: 15%;" src="<c:url value="/assets/end-user/images/img/Aaron Ward.jpg"/>">
												</a>
												<div class="media-body ">
													<small class="pull-right">5h ago</small>
													<strong>Aaron Ward</strong> gave you a <strong> Thank You Award</strong>.
													<br>
												</div>
											</div>
										</li>
										<li class="divider"></li>
									</ul>
								</li>
								<li class="dropdown text-center">
									<a data-toggle="dropdown" class="dropdown-toggle" href="#" style="width: 180px;">
									<img alt="" src="<c:url value="/restapi/getProfilePicture?id=${loggedInUser.id}"/>" class="img-circle profile-img thumb-sm">
									<span class="username">${loggedInUser.firstName} ${loggedInUser.lastName}</span> <span class="caret"></span>
									</a>
									<ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
										<li><a href="profile.html"><i class="fa fa-user"></i>Profile</a></li>
										<li><a href="#"><i class="fa fa-camera"></i> Change Profile Picture </a></li>
										<li><a data-toggle="modal" href="#modal-form"><i class="fa fa-cog"></i> Change Password</a></li>
										<div id="modal-form" class="modal fade" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-body">
														<div class="row">
															<div class="col-sm-12">
																<h3 class="m-t-none m-b">Change password</h3>
																<form role="form">
																	<div class="form-group">
																		<label>Current Password</label>
																		<input type="password" placeholder="Current password" class="form-control">
																	</div>
																	<div class="form-group">
																		<label>New Password</label> 
																		<input type="password" placeholder="New password" class="form-control">
																	</div>
																	<div class="form-group">
																		<label>Confirm new Password</label>
																		<input type="password" placeholder="Confirm new password" class="form-control">
																	</div>
																	<div>
																		<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<li><a href="${logoutUrl}"><i class="fa fa-sign-out"></i> Log Out</a></li>
									</ul>
								</li>
							</ul>
						</nav>
		            </div>
		            <!-- End Top Nav Bar -->
		            <!-- iBox Status -->
		            <c:if test="${isDashboard}">
			            <div class="ibox-status " style="height: 210px; width: 777px;">
							<div class="social-status">
								<div class="row box">
									<p class="font-extra-bold font-uppercase col-md-4 col-md-offset-1 text-center tab-switch tab-switch-active"
										style="float: left" id="whats-happening-tigger">
										<i class="fa fa-commenting" style="padding-right: 6px;"></i> What's Happening
									</p>
									<p class="font-extra-bold font-uppercase col-md-6 text-center tab-switch" id="recognize-trigger">
										<i class="fa fa-bookmark" style="padding-right: 6px;"></i> Recognize
									</p>
									<div class="col-md-12 whats-happening">
										<div class="content">
											<div class="col-md-12">
												<!--<select class="form-control js-data-example-ajax_1 form-control themes-dark" placeholder="Tell us what's new" multiple></select>-->
												<textarea class="form-control" rows="10" placeholder="Tell us what's new..."></textarea>
											</div>
										</div>
										<div class="col-md-12">
											<a href="#" class="btn pick-photo" style="color: #676A6C; padding-top: 20px;">
											<i class="fa fa-camera" style="padding-right: 6px;"></i> Add Photos
											</a>
											<button class="btn btn-sm btn-primary pull-right m-t-n-xs submit-message" type="submit">
											<strong>Post</strong>
											</button>
										</div>
									</div>
									<div class="col-md-12 recognize">
										<div class="content">
											<div class="col-md-12">
												<div id="myCarousel" class="carousel slide" data-ride="carousel">
													<div class="carousel-inner" role="listbox">
														<div class="item active">
															<div class="first-row">
																<input type="text" id="input-recognize" class="input-tags demo-default movies" placeholder="Who do you want to Recognize?"/>
															</div>
															<div class="second-row">
																<label for="check" class="check-manager">
																<input type="checkbox" name="iCheck" id="check"/>Share with
																their Manager
																</label>
															</div>
														</div>
														<div class="item">
															<div class="first-row">
																<textarea class="form-control" id="message" rows="10" placeholder="Message"></textarea>
															</div>
															<div class="second-row">
																<div class="block">
																	<input type="text" id="input-tags" class="input-tags demo-default movies" placeholder="Who else should know?"/>
																</div>
															</div>
														</div>
														<input type="hidden" value="${memberId}" id="memberId">
														<div class="item">
															<div class="first-row">
																<div class="award-target"></div>
															</div>
															<div class="second-row">
																<div class="col-md-12">
																	<a href="#" id="pick-award" class="btn btn-outline btn-primary">Select
																	an Award</a>
																	<a href= "javascript:getRecognizeUser();"
																		class="btn btn-sm btn-primary pulll-right m-t-n-xs recoginize-button">
																	<strong>Recognize</strong></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="step-indicator">
													<span class="screen-count">1 / 3</span>
												</div>
												<div class="nav-controls">
													<a class="left carousel-control" href="#myCarousel" role="button"
														data-slide="prev">
													<i class="btn arrow-btn-back"></i>
													</a>
													<a class="right carousel-control" href="#myCarousel" role="button"
														data-slide="next">
													<i class="btn arrow-btn"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:if>
					<!-- End iBox Status -->
		        </div>
	        </sec:authorize>
	        <!-- End Top Nav Bar + iBox Status -->
			<%@ include file="/messages.jsp"%>
			<decorator:body />
			<br>
		</div>

		<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="modalLabel">Crop the image</h4>
					</div>
					<div class="modal-body">
						<img src="img/Jason Thomas Profile Pic.jpeg" id="imagepreview" style="width: 0px">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
