/**
 * Created by root on 13/10/15.
 */
$(function () {


    $('#recognize-trigger').click(function () {
        $('.whats-happening, #whats-controls').fadeOut('fast', function () {
            $('.recognize').fadeIn();
            $('#recognize-controls').fadeIn();
        });

        $('#whats-happening-tigger').removeClass('tab-switch-active');
        $(this).addClass('tab-switch-active');
    });

    $('#whats-happening-tigger').click(function () {
        $('.recognize, #recognize-controls').fadeOut('fast', function () {
            $('.whats-happening').fadeIn();
            $('#whats-controls').fadeIn();

        });

        $('#recognize-trigger').removeClass('tab-switch-active');
        $(this).addClass('tab-switch-active');
    });


    function formatRepo(repo) {
        if (repo.loading) return repo.text;

        var markup = '<div class="clearfix">' +
            '<div class="col-sm-1">' +
            '<img src="' + repo.owner.avatar_url + '" style="max-width: 100%" />' +
            '</div>' +
            '<div clas="col-sm-10">' +
            '<div class="clearfix">' +
            '<div class="col-sm-6">' + repo.full_name + '</div>' +
            '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + repo.forks_count + '</div>' +
            '<div class="col-sm-2"><i class="fa fa-star"></i> ' + repo.stargazers_count + '</div>' +
            '</div>';

        if (repo.description) {
            markup += '<div>' + repo.description + '</div>';
        }

        markup += '</div></div>';

        return markup;
    }

    function formatRepoSelection(repo) {
        return '<span>' +
            '<img src="#" class="profile-pic"/>' +
            '<b>' + (repo.full_name || repo.text) + '</b>' +
            '</span>';
    }

    $(".js-data-example-ajax").select2({
        theme: "bootstrap",
        allowClear: true,
        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });


    window.isSideBarVisible = false;
    window.slide = 1;

    $('#myCarousel').carousel({
        interval: false,
        keyboard: false
    })

    var totalSteps = 2;

    var stepCount = 1;

    $('.left').hide();

    $('#myCarousel').bind('slide.bs.carousel', function (event) {
        var index = $('.carousel-inner .active').index();
        if (event.direction === 'left') {
            if (index == totalSteps) {
                return false;
            }
            stepCount++;
        }

        if (event.direction === 'right') {
            if (index == 0) {
                return false;
            }
            stepCount--;
        }

        if (stepCount === 1) {
            $('.left').hide();
            $('.right').show();
        } else if (stepCount) {
            $('.left').show();
        }
        if (stepCount >= 3) {
            $('.left').show();
            $('.right').hide();

            setTimeout(function () {
                $('#slide').stop().animate({right: 0}, 500);
                isSideBarVisible = true;
                $('body').append('<div class="modal-backdrop fade in"></div>');

                $('#pick-award').show();
            }, 300);

        } else if (stepCount) {
            $('.right').show();

            $('#slide').stop().animate({right: -400}, 500);
            isSideBarVisible = false;
            $('#pick-award').hide();

            $('.modal-backdrop').remove();
        }

        $('.screen-count').html(stepCount + ' / 3');

    });

    $("body").click(function () {
        if (isSideBarVisible) {
            var width = $(this).width();// -10;
            $('#slide').stop().animate({right: -400}, 500);
            isSideBarVisible = false;
            $('.modal-backdrop').remove();
        }
    });


    $('.award-item').click(function () {
        var $target = $('.award-target');

        var src = $(this).find('img').prop('src');
        var label = $(this).find('.award-title').text();
        var desc = $(this).find('.award-points').text();


        var $html = $('<ul class="award-list result">' +
            '<li class="current">' +
            '<img src="' + src + '"/>' +
            '<div class="award-result"><span class="award-title">' + label + '</span>' +
            '<span class="award-point">' + desc + '</span> </div>' +
            '</li>');

        $target.html($html);
    });

    $('#pick-award').click(function () {
        $('#slide').stop().animate({right: 0}, 500, function () {
            isSideBarVisible = true;
        });

        $('body').append('<div class="modal-backdrop fade in"></div>');
    });


    $('#page-bg').mCustomScrollbar({
        axis: "x",
        theme: 'dark'
    });

    $(window).resize(function(){
        setTimeout(function(){
            hideScrollbars();
        }, 300);
    });

});

